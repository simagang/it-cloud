# IT-CLOUD系统API服务


<a name="overview"></a>
## 概览
IT-CLOUD系统API接口文档


### 版本信息
*版本* : v1


### 联系方式
*名字* : 司马缸砸缸了  
*邮箱* : gaha_hero@163.com


### URI scheme
*域名* : localhost:8081  
*基础路径* : /  
*方案* : HTTP, HTTPS


### 标签

* OSS : Oss Resource Controller
* act-manage-controller : Act Manage Controller
* feign测试 : Feign Controller
* role-menu-controller : Role Menu Controller
* socket-controller : Socket Controller
* user-role-controller : User Role Controller
* 任务日志 : Schedule Job Log Controller
* 模型 : Act Re Model Controller
* 流程定义 : Act Re Procdef Controller
* 用户 : User Controller
* 登陆 : Login Controller
* 系统日志 : Sys Log Controller
* 菜单 : Menu Controller
* 角色 : Role Controller
* 请假 : Leave Controller
* 调度 : Schedule Job Controller


### 消耗

* `application/json`


### 生成

* `application/json`



