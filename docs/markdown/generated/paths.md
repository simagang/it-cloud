
<a name="paths"></a>
## 资源

<a name="oss_resource"></a>
### OSS
Oss Resource Controller


<a name="listusingget_4"></a>
#### 分页查询接口
```
GET /admin/oss
```


##### 说明
条件，分页查询


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**params**  <br>*必填*|params|object|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/oss
```


###### 请求 query
```
json :
{
  "params" : "object"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="deleteusingpost_2"></a>
#### 上传文件接口
```
POST /admin/oss/delete
```


##### 说明
上传文件


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**ids**  <br>*必填*|ids|< string > array|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/oss/delete
```


###### 请求 body
```
json :
[ "string" ]
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="uploadcoverusingpost"></a>
#### 上传文件接口
```
POST /admin/oss/upload
```


##### 说明
上传文件


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**FormData**|**file**  <br>*可选*|file|file|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `multipart/form-data`


##### HTTP请求示例

###### 请求 path
```
/admin/oss/upload
```


###### 请求 formData
```
json :
"file"
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="act-manage-controller_resource"></a>
### Act-manage-controller
Act Manage Controller


<a name="getstencilsetusingpost"></a>
#### getStencilset
```
POST /service/editor/stencilset
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/editor/stencilset
```


##### HTTP响应示例

###### 响应 200
```
json :
"string"
```


<a name="getstencilsetusingget"></a>
#### getStencilset
```
GET /service/editor/stencilset
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/editor/stencilset
```


##### HTTP响应示例

###### 响应 200
```
json :
"string"
```


<a name="getstencilsetusingput"></a>
#### getStencilset
```
PUT /service/editor/stencilset
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/editor/stencilset
```


##### HTTP响应示例

###### 响应 200
```
json :
"string"
```


<a name="getstencilsetusingdelete"></a>
#### getStencilset
```
DELETE /service/editor/stencilset
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/editor/stencilset
```


##### HTTP响应示例

###### 响应 200
```
json :
"string"
```


<a name="getstencilsetusingpatch"></a>
#### getStencilset
```
PATCH /service/editor/stencilset
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/editor/stencilset
```


##### HTTP响应示例

###### 响应 200
```
json :
"string"
```


<a name="getstencilsetusinghead"></a>
#### getStencilset
```
HEAD /service/editor/stencilset
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/editor/stencilset
```


##### HTTP响应示例

###### 响应 200
```
json :
"string"
```


<a name="getstencilsetusingoptions"></a>
#### getStencilset
```
OPTIONS /service/editor/stencilset
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/editor/stencilset
```


##### HTTP响应示例

###### 响应 200
```
json :
"string"
```


<a name="geteditorjsonusingpost"></a>
#### getEditorJson
```
POST /service/model/{modelId}/json
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ObjectNode](#objectnode)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/json
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "array" : true,
  "bigDecimal" : true,
  "bigInteger" : true,
  "binary" : true,
  "boolean" : true,
  "containerNode" : true,
  "double" : true,
  "float" : true,
  "floatingPointNumber" : true,
  "int" : true,
  "integralNumber" : true,
  "long" : true,
  "missingNode" : true,
  "nodeType" : "string",
  "null" : true,
  "number" : true,
  "object" : true,
  "pojo" : true,
  "short" : true,
  "textual" : true,
  "valueNode" : true
}
```


<a name="geteditorjsonusingget"></a>
#### getEditorJson
```
GET /service/model/{modelId}/json
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ObjectNode](#objectnode)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/json
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "array" : true,
  "bigDecimal" : true,
  "bigInteger" : true,
  "binary" : true,
  "boolean" : true,
  "containerNode" : true,
  "double" : true,
  "float" : true,
  "floatingPointNumber" : true,
  "int" : true,
  "integralNumber" : true,
  "long" : true,
  "missingNode" : true,
  "nodeType" : "string",
  "null" : true,
  "number" : true,
  "object" : true,
  "pojo" : true,
  "short" : true,
  "textual" : true,
  "valueNode" : true
}
```


<a name="geteditorjsonusingput"></a>
#### getEditorJson
```
PUT /service/model/{modelId}/json
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ObjectNode](#objectnode)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/json
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "array" : true,
  "bigDecimal" : true,
  "bigInteger" : true,
  "binary" : true,
  "boolean" : true,
  "containerNode" : true,
  "double" : true,
  "float" : true,
  "floatingPointNumber" : true,
  "int" : true,
  "integralNumber" : true,
  "long" : true,
  "missingNode" : true,
  "nodeType" : "string",
  "null" : true,
  "number" : true,
  "object" : true,
  "pojo" : true,
  "short" : true,
  "textual" : true,
  "valueNode" : true
}
```


<a name="geteditorjsonusingdelete"></a>
#### getEditorJson
```
DELETE /service/model/{modelId}/json
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ObjectNode](#objectnode)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/json
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "array" : true,
  "bigDecimal" : true,
  "bigInteger" : true,
  "binary" : true,
  "boolean" : true,
  "containerNode" : true,
  "double" : true,
  "float" : true,
  "floatingPointNumber" : true,
  "int" : true,
  "integralNumber" : true,
  "long" : true,
  "missingNode" : true,
  "nodeType" : "string",
  "null" : true,
  "number" : true,
  "object" : true,
  "pojo" : true,
  "short" : true,
  "textual" : true,
  "valueNode" : true
}
```


<a name="geteditorjsonusingpatch"></a>
#### getEditorJson
```
PATCH /service/model/{modelId}/json
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ObjectNode](#objectnode)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/json
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "array" : true,
  "bigDecimal" : true,
  "bigInteger" : true,
  "binary" : true,
  "boolean" : true,
  "containerNode" : true,
  "double" : true,
  "float" : true,
  "floatingPointNumber" : true,
  "int" : true,
  "integralNumber" : true,
  "long" : true,
  "missingNode" : true,
  "nodeType" : "string",
  "null" : true,
  "number" : true,
  "object" : true,
  "pojo" : true,
  "short" : true,
  "textual" : true,
  "valueNode" : true
}
```


<a name="geteditorjsonusinghead"></a>
#### getEditorJson
```
HEAD /service/model/{modelId}/json
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ObjectNode](#objectnode)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/json
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "array" : true,
  "bigDecimal" : true,
  "bigInteger" : true,
  "binary" : true,
  "boolean" : true,
  "containerNode" : true,
  "double" : true,
  "float" : true,
  "floatingPointNumber" : true,
  "int" : true,
  "integralNumber" : true,
  "long" : true,
  "missingNode" : true,
  "nodeType" : "string",
  "null" : true,
  "number" : true,
  "object" : true,
  "pojo" : true,
  "short" : true,
  "textual" : true,
  "valueNode" : true
}
```


<a name="geteditorjsonusingoptions"></a>
#### getEditorJson
```
OPTIONS /service/model/{modelId}/json
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ObjectNode](#objectnode)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/json
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "array" : true,
  "bigDecimal" : true,
  "bigInteger" : true,
  "binary" : true,
  "boolean" : true,
  "containerNode" : true,
  "double" : true,
  "float" : true,
  "floatingPointNumber" : true,
  "int" : true,
  "integralNumber" : true,
  "long" : true,
  "missingNode" : true,
  "nodeType" : "string",
  "null" : true,
  "number" : true,
  "object" : true,
  "pojo" : true,
  "short" : true,
  "textual" : true,
  "valueNode" : true
}
```


<a name="savemodelusingpost"></a>
#### saveModel
```
POST /service/model/{modelId}/save
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|
|**Query**|**description**  <br>*必填*|description|string|
|**Query**|**json_xml**  <br>*必填*|json_xml|string|
|**Query**|**name**  <br>*必填*|name|string|
|**Query**|**svg_xml**  <br>*必填*|svg_xml|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/save
```


###### 请求 query
```
json :
{
  "description" : "string",
  "json_xml" : "string",
  "name" : "string",
  "svg_xml" : "string"
}
```


<a name="savemodelusingget"></a>
#### saveModel
```
GET /service/model/{modelId}/save
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|
|**Query**|**description**  <br>*必填*|description|string|
|**Query**|**json_xml**  <br>*必填*|json_xml|string|
|**Query**|**name**  <br>*必填*|name|string|
|**Query**|**svg_xml**  <br>*必填*|svg_xml|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/save
```


###### 请求 query
```
json :
{
  "description" : "string",
  "json_xml" : "string",
  "name" : "string",
  "svg_xml" : "string"
}
```


<a name="savemodelusingput"></a>
#### saveModel
```
PUT /service/model/{modelId}/save
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|
|**Query**|**description**  <br>*必填*|description|string|
|**Query**|**json_xml**  <br>*必填*|json_xml|string|
|**Query**|**name**  <br>*必填*|name|string|
|**Query**|**svg_xml**  <br>*必填*|svg_xml|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/save
```


###### 请求 query
```
json :
{
  "description" : "string",
  "json_xml" : "string",
  "name" : "string",
  "svg_xml" : "string"
}
```


<a name="savemodelusingdelete"></a>
#### saveModel
```
DELETE /service/model/{modelId}/save
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|
|**Query**|**description**  <br>*必填*|description|string|
|**Query**|**json_xml**  <br>*必填*|json_xml|string|
|**Query**|**name**  <br>*必填*|name|string|
|**Query**|**svg_xml**  <br>*必填*|svg_xml|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/save
```


###### 请求 query
```
json :
{
  "description" : "string",
  "json_xml" : "string",
  "name" : "string",
  "svg_xml" : "string"
}
```


<a name="savemodelusingpatch"></a>
#### saveModel
```
PATCH /service/model/{modelId}/save
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|
|**Query**|**description**  <br>*必填*|description|string|
|**Query**|**json_xml**  <br>*必填*|json_xml|string|
|**Query**|**name**  <br>*必填*|name|string|
|**Query**|**svg_xml**  <br>*必填*|svg_xml|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/save
```


###### 请求 query
```
json :
{
  "description" : "string",
  "json_xml" : "string",
  "name" : "string",
  "svg_xml" : "string"
}
```


<a name="savemodelusinghead"></a>
#### saveModel
```
HEAD /service/model/{modelId}/save
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|
|**Query**|**description**  <br>*必填*|description|string|
|**Query**|**json_xml**  <br>*必填*|json_xml|string|
|**Query**|**name**  <br>*必填*|name|string|
|**Query**|**svg_xml**  <br>*必填*|svg_xml|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/save
```


###### 请求 query
```
json :
{
  "description" : "string",
  "json_xml" : "string",
  "name" : "string",
  "svg_xml" : "string"
}
```


<a name="savemodelusingoptions"></a>
#### saveModel
```
OPTIONS /service/model/{modelId}/save
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**modelId**  <br>*必填*|modelId|string|
|**Query**|**description**  <br>*必填*|description|string|
|**Query**|**json_xml**  <br>*必填*|json_xml|string|
|**Query**|**name**  <br>*必填*|name|string|
|**Query**|**svg_xml**  <br>*必填*|svg_xml|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/service/model/string/save
```


###### 请求 query
```
json :
{
  "description" : "string",
  "json_xml" : "string",
  "name" : "string",
  "svg_xml" : "string"
}
```


<a name="31b29cfa2497650e7ffbe1bf04dc0291"></a>
### Feign测试
Feign Controller


<a name="hiusingget"></a>
#### 测试
```
GET /feign/test
```


##### 说明
测试


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**name**  <br>*必填*|name|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/feign/test
```


###### 请求 query
```
json :
{
  "name" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
"string"
```


<a name="role-menu-controller_resource"></a>
### Role-menu-controller
Role Menu Controller


<a name="allotusingpost"></a>
#### 分配角色
```
POST /admin/roleMenu/allot
```


##### 说明
分配角色


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**roleMenuDTO**  <br>*必填*|RoleMenuDTO实体类|[RoleMenuDTO](#rolemenudto)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/roleMenu/allot
```


###### 请求 body
```
json :
{
  "menuList" : [ "string" ],
  "roleId" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="socket-controller_resource"></a>
### Socket-controller
Socket Controller


<a name="pushtowebusingpost"></a>
#### pushToWeb
```
POST /socket/push/{userId}
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**userId**  <br>*必填*|userId|string|
|**Query**|**message**  <br>*可选*|message|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/socket/push/string
```


###### 请求 query
```
json :
{
  "message" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="pushtowebusingget"></a>
#### pushToWeb
```
GET /socket/push/{userId}
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**userId**  <br>*必填*|userId|string|
|**Query**|**message**  <br>*可选*|message|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/socket/push/string
```


###### 请求 query
```
json :
{
  "message" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="pushtowebusingput"></a>
#### pushToWeb
```
PUT /socket/push/{userId}
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**userId**  <br>*必填*|userId|string|
|**Query**|**message**  <br>*可选*|message|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/socket/push/string
```


###### 请求 query
```
json :
{
  "message" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="pushtowebusingdelete"></a>
#### pushToWeb
```
DELETE /socket/push/{userId}
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**userId**  <br>*必填*|userId|string|
|**Query**|**message**  <br>*可选*|message|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/socket/push/string
```


###### 请求 query
```
json :
{
  "message" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="pushtowebusingpatch"></a>
#### pushToWeb
```
PATCH /socket/push/{userId}
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**userId**  <br>*必填*|userId|string|
|**Query**|**message**  <br>*可选*|message|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/socket/push/string
```


###### 请求 query
```
json :
{
  "message" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="pushtowebusinghead"></a>
#### pushToWeb
```
HEAD /socket/push/{userId}
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**userId**  <br>*必填*|userId|string|
|**Query**|**message**  <br>*可选*|message|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/socket/push/string
```


###### 请求 query
```
json :
{
  "message" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="pushtowebusingoptions"></a>
#### pushToWeb
```
OPTIONS /socket/push/{userId}
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**userId**  <br>*必填*|userId|string|
|**Query**|**message**  <br>*可选*|message|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/socket/push/string
```


###### 请求 query
```
json :
{
  "message" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="socketusingget"></a>
#### socket
```
GET /socket/{userId}
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**userId**  <br>*必填*|userId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ModelAndView](#modelandview)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/socket/string
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "empty" : true,
  "model" : "object",
  "modelMap" : {
    "string" : "object"
  },
  "reference" : true,
  "status" : "string",
  "view" : {
    "contentType" : "string"
  },
  "viewName" : "string"
}
```


<a name="user-role-controller_resource"></a>
### User-role-controller
User Role Controller


<a name="allotusingpost_1"></a>
#### 分配角色
```
POST /admin/userRole/allot
```


##### 说明
分配角色


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**userRoleDTO**  <br>*必填*|UserRoleDTO实体类|[UserRoleDTO](#userroledto)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/userRole/allot
```


###### 请求 body
```
json :
{
  "roleList" : [ "string" ],
  "userId" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="b929a80b71dd9d9aed7d1032cfd8a4c6"></a>
### 任务日志
Schedule Job Log Controller


<a name="listusingget_7"></a>
#### 分页查询job日志接口
```
GET /admin/schedule/log
```


##### 说明
条件，分页查询


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**params**  <br>*必填*|params|object|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/schedule/log
```


###### 请求 query
```
json :
{
  "params" : "object"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="infousingget_4"></a>
#### job日志信息接口
```
GET /admin/schedule/log/{id}
```


##### 说明
job日志信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|id|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/schedule/log/string
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="5eadc0e5c6a5aecd16c74bc3a3d133b4"></a>
### 模型
Act Re Model Controller


<a name="saveusingpost"></a>
#### 新增
```
POST /act/model
```


##### 说明
新增


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**actReModel**  <br>*必填*|actReModel|[ActReModelEntity](#actremodelentity)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/act/model
```


###### 请求 body
```
json :
{
  "category" : "string",
  "createTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "deploymentId" : "string",
  "description" : "string",
  "editorSourceExtraValueId" : "string",
  "editorSourceValueId" : "string",
  "id" : "string",
  "key" : "string",
  "lastUpdateTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "metaInfo" : "string",
  "name" : "string",
  "rev" : 0,
  "tenantId" : "string",
  "version" : 0
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="deleteusingpost"></a>
#### 批量删除
```
POST /act/model/delete
```


##### 说明
批量删除


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**ids**  <br>*必填*|ids|< string > array|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/act/model/delete
```


###### 请求 body
```
json :
[ "string" ]
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="deployusingget"></a>
#### 部署
```
GET /act/model/deploy
```


##### 说明
部署


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**id**  <br>*可选*|id|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/act/model/deploy
```


###### 请求 query
```
json :
{
  "id" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="exportusingget"></a>
#### 导出model的xml文件
```
GET /act/model/export
```


##### 说明
导出model的xml文件


##### 参数

|类型|名称|说明|类型|默认值|
|---|---|---|---|---|
|**Query**|**id**  <br>*可选*|id|string||
|**Query**|**type**  <br>*可选*|type|string|`"xml"`|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/act/model/export
```


###### 请求 query
```
json :
{
  "id" : "string",
  "type" : "string"
}
```


<a name="listusingget"></a>
#### 分页查询接口
```
GET /act/model/page
```


##### 说明
条件，分页查询


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**params**  <br>*必填*|params|object|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/act/model/page
```


###### 请求 query
```
json :
{
  "params" : "object"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="30d9f754bdb9b290ae4d88f71b5d2391"></a>
### 流程定义
Act Re Procdef Controller


<a name="converttomodelusingget"></a>
#### 转模型
```
GET /act/procdef/convertToModel
```


##### 说明
转模型


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**id**  <br>*可选*|id|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/act/procdef/convertToModel
```


###### 请求 query
```
json :
{
  "id" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="deleteusingpost_1"></a>
#### 删除流程定义
```
POST /act/procdef/delete
```


##### 说明
删除流程定义(部署)


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**deploymentIds**  <br>*必填*|deploymentIds|< string > array|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/act/procdef/delete
```


###### 请求 body
```
json :
[ "string" ]
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="deployusingpost"></a>
#### 部署流程文件
```
POST /act/procdef/deploy
```


##### 说明
部署流程文件


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**FormData**|**file**  <br>*可选*|file|file|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `multipart/form-data`


##### HTTP请求示例

###### 请求 path
```
/act/procdef/deploy
```


###### 请求 formData
```
json :
"file"
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="listusingget_1"></a>
#### 分页查询接口
```
GET /act/procdef/page
```


##### 说明
条件，分页查询


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**params**  <br>*必填*|params|object|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/act/procdef/page
```


###### 请求 query
```
json :
{
  "params" : "object"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="resourcereadusingget"></a>
#### 读取资源
```
GET /act/procdef/read
```


##### 说明
读取资源,通过ProcessDefinitionId


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**id**  <br>*可选*|id|string|
|**Query**|**proInsId**  <br>*可选*|proInsId|string|
|**Query**|**type**  <br>*可选*|type|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/act/procdef/read
```


###### 请求 query
```
json :
{
  "id" : "string",
  "proInsId" : "string",
  "type" : "string"
}
```


<a name="startprocessinstancebyidusingget"></a>
#### 启动流程实例
```
GET /act/procdef/startProcessInstance
```


##### 说明
启动流程实例，通过processDefinitionId


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**processDefinitionId**  <br>*可选*|processDefinitionId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/act/procdef/startProcessInstance
```


###### 请求 query
```
json :
{
  "processDefinitionId" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="updateusingput"></a>
#### 激活/挂起
```
PUT /act/procdef/status
```


##### 说明
激活/挂起


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**id**  <br>*可选*|id|string|
|**Query**|**state**  <br>*可选*|state|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/act/procdef/status
```


###### 请求 query
```
json :
{
  "id" : "string",
  "state" : 0
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="6352f1a072c12b600cd8669cd91a062d"></a>
### 用户
User Controller


<a name="saveusingpost_5"></a>
#### 添加用户
```
POST /admin/user
```


##### 说明
添加用户


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**userEntity**  <br>*必填*|UserEntity实体类|[UserEntity](#userentity)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/user
```


###### 请求 body
```
json :
{
  "age" : 0,
  "createTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "createUser" : "string",
  "deleteStatus" : 0,
  "email" : "string",
  "id" : "string",
  "mobile" : "string",
  "password" : "string",
  "roleList" : [ {
    "createTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "createUser" : "string",
    "id" : "string",
    "menuList" : [ {
      "createTime" : {
        "date" : 0,
        "day" : 0,
        "hours" : 0,
        "minutes" : 0,
        "month" : 0,
        "nanos" : 0,
        "seconds" : 0,
        "time" : 0,
        "timezoneOffset" : 0,
        "year" : 0
      },
      "createUser" : "string",
      "icon" : "string",
      "id" : "string",
      "list" : [ "object" ],
      "name" : "string",
      "open" : false,
      "orderNum" : 0,
      "parentId" : "string",
      "parentName" : "string",
      "perms" : "string",
      "type" : 0,
      "url" : "string"
    } ],
    "remark" : "string",
    "roleName" : "string"
  } ],
  "salt" : "string",
  "sex" : "string",
  "status" : 0,
  "updateTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "updateUser" : "string",
  "username" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="getpageusingget"></a>
#### 分页查询用户接口
```
GET /admin/user
```


##### 说明
条件，分页查询


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**params**  <br>*可选*|请求过滤参数|object|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/user
```


###### 请求 query
```
json :
{
  "params" : "object"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="updateusingput_5"></a>
#### 更新用户
```
PUT /admin/user
```


##### 说明
更新用户


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**userEntity**  <br>*必填*|userEntity|[UserEntity](#userentity)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/user
```


###### 请求 body
```
json :
{
  "age" : 0,
  "createTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "createUser" : "string",
  "deleteStatus" : 0,
  "email" : "string",
  "id" : "string",
  "mobile" : "string",
  "password" : "string",
  "roleList" : [ {
    "createTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "createUser" : "string",
    "id" : "string",
    "menuList" : [ {
      "createTime" : {
        "date" : 0,
        "day" : 0,
        "hours" : 0,
        "minutes" : 0,
        "month" : 0,
        "nanos" : 0,
        "seconds" : 0,
        "time" : 0,
        "timezoneOffset" : 0,
        "year" : 0
      },
      "createUser" : "string",
      "icon" : "string",
      "id" : "string",
      "list" : [ "object" ],
      "name" : "string",
      "open" : false,
      "orderNum" : 0,
      "parentId" : "string",
      "parentName" : "string",
      "perms" : "string",
      "type" : 0,
      "url" : "string"
    } ],
    "remark" : "string",
    "roleName" : "string"
  } ],
  "salt" : "string",
  "sex" : "string",
  "status" : 0,
  "updateTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "updateUser" : "string",
  "username" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="getlistusingget"></a>
#### 条件查询用户接口
```
GET /admin/user/list
```


##### 说明
条件，不分页查询


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**age**  <br>*可选*||integer (int32)|
|**Query**|**createTime.nanos**  <br>*可选*||integer (int32)|
|**Query**|**createUser**  <br>*可选*||string|
|**Query**|**deleteStatus**  <br>*可选*||integer (int32)|
|**Query**|**email**  <br>*可选*||string|
|**Query**|**id**  <br>*可选*||string|
|**Query**|**mobile**  <br>*可选*||string|
|**Query**|**password**  <br>*可选*||string|
|**Query**|**roleList[0].createTime.nanos**  <br>*可选*||integer (int32)|
|**Query**|**roleList[0].createUser**  <br>*可选*||string|
|**Query**|**roleList[0].id**  <br>*可选*||string|
|**Query**|**roleList[0].menuList[0].createTime.nanos**  <br>*可选*||integer (int32)|
|**Query**|**roleList[0].menuList[0].createUser**  <br>*可选*||string|
|**Query**|**roleList[0].menuList[0].icon**  <br>*可选*|菜单图标|string|
|**Query**|**roleList[0].menuList[0].id**  <br>*可选*||string|
|**Query**|**roleList[0].menuList[0].list**  <br>*可选*|目录-菜单集合|< object > array(multi)|
|**Query**|**roleList[0].menuList[0].name**  <br>*可选*|菜单名称|string|
|**Query**|**roleList[0].menuList[0].open**  <br>*可选*|z-tree属性|boolean|
|**Query**|**roleList[0].menuList[0].orderNum**  <br>*可选*|排序|integer (int32)|
|**Query**|**roleList[0].menuList[0].parentId**  <br>*可选*|父菜单ID，一级菜单为0|string|
|**Query**|**roleList[0].menuList[0].parentName**  <br>*可选*|父菜单名称|string|
|**Query**|**roleList[0].menuList[0].perms**  <br>*可选*|授权(多个用逗号分隔，如：user:list,user:create)|string|
|**Query**|**roleList[0].menuList[0].type**  <br>*可选*|类型   0：目录   1：菜单   2：按钮|integer (int32)|
|**Query**|**roleList[0].menuList[0].url**  <br>*可选*|菜单URL|string|
|**Query**|**roleList[0].remark**  <br>*可选*||string|
|**Query**|**roleList[0].roleName**  <br>*可选*||string|
|**Query**|**salt**  <br>*可选*||string|
|**Query**|**sex**  <br>*可选*||string|
|**Query**|**status**  <br>*可选*||integer (int32)|
|**Query**|**updateTime.nanos**  <br>*可选*||integer (int32)|
|**Query**|**updateUser**  <br>*可选*||string|
|**Query**|**username**  <br>*可选*||string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/user/list
```


###### 请求 query
```
json :
{
  "age" : 0,
  "createTime.nanos" : 0,
  "createUser" : "string",
  "deleteStatus" : 0,
  "email" : "string",
  "id" : "string",
  "mobile" : "string",
  "password" : "string",
  "roleList[0].createTime.nanos" : 0,
  "roleList[0].createUser" : "string",
  "roleList[0].id" : "string",
  "roleList[0].menuList[0].createTime.nanos" : 0,
  "roleList[0].menuList[0].createUser" : "string",
  "roleList[0].menuList[0].icon" : "string",
  "roleList[0].menuList[0].id" : "string",
  "roleList[0].menuList[0].list" : "object",
  "roleList[0].menuList[0].name" : "string",
  "roleList[0].menuList[0].open" : true,
  "roleList[0].menuList[0].orderNum" : 0,
  "roleList[0].menuList[0].parentId" : "string",
  "roleList[0].menuList[0].parentName" : "string",
  "roleList[0].menuList[0].perms" : "string",
  "roleList[0].menuList[0].type" : 0,
  "roleList[0].menuList[0].url" : "string",
  "roleList[0].remark" : "string",
  "roleList[0].roleName" : "string",
  "salt" : "string",
  "sex" : "string",
  "status" : 0,
  "updateTime.nanos" : 0,
  "updateUser" : "string",
  "username" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="getuserusingget"></a>
#### ID查询用户
```
GET /admin/user/{id}
```


##### 说明
根据ID查询用户


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|主键Id|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/user/string
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="deleteusingdelete_2"></a>
#### 删除用户
```
DELETE /admin/user/{id}
```


##### 说明
删除用户


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|ID主键|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/user/string
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="2c79386e193457ae771f9152cff0cd04"></a>
### 登陆
Login Controller


<a name="loginusingpost"></a>
#### 登陆
```
POST /admin/login
```


##### 说明
登陆


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**form**  <br>*必填*|form|[登录表单对象](#fea127974cda404395799413a0680320)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/login
```


###### 请求 body
```
json :
{
  "captcha" : "string",
  "password" : "string",
  "username" : "string",
  "uuid" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="logoutusingpost"></a>
#### 注销
```
POST /admin/logout
```


##### 说明
注销


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/logout
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="captchausingget"></a>
#### 分页查询用户接口
```
GET /captcha.jpg
```


##### 说明
条件，分页查询


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**uuid**  <br>*可选*|uuid|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/captcha.jpg
```


###### 请求 query
```
json :
{
  "uuid" : "string"
}
```


<a name="passwordusingpost"></a>
#### 生成密码
```
POST /password
```


##### 说明
生成密码


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**password**  <br>*必填*|password|string|
|**Query**|**salt**  <br>*必填*|salt|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/password
```


###### 请求 query
```
json :
{
  "password" : "string",
  "salt" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="84053bf994f679ff9845263d02307c82"></a>
### 系统日志
Sys Log Controller


<a name="listusingget_8"></a>
#### 分页查询日志接口
```
GET /system/log
```


##### 说明
条件，分页查询


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**params**  <br>*必填*|params|object|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/system/log
```


###### 请求 query
```
json :
{
  "params" : "object"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="806a2549f0a8d0d2d2ff18c99475ea5c"></a>
### 菜单
Menu Controller


<a name="saveusingpost_2"></a>
#### 保存
```
POST /admin/menu
```


##### 说明
保存


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**menu**  <br>*必填*|menu|[MenuEntity](#menuentity)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/menu
```


###### 请求 body
```
json :
{
  "createTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "createUser" : "string",
  "icon" : "string",
  "id" : "string",
  "list" : [ "object" ],
  "name" : "string",
  "open" : false,
  "orderNum" : 0,
  "parentId" : "string",
  "parentName" : "string",
  "perms" : "string",
  "type" : 0,
  "url" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="updateusingput_2"></a>
#### 修改
```
PUT /admin/menu
```


##### 说明
修改


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**menu**  <br>*必填*|menu|[MenuEntity](#menuentity)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/menu
```


###### 请求 body
```
json :
{
  "createTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "createUser" : "string",
  "icon" : "string",
  "id" : "string",
  "list" : [ "object" ],
  "name" : "string",
  "open" : false,
  "orderNum" : 0,
  "parentId" : "string",
  "parentName" : "string",
  "perms" : "string",
  "type" : 0,
  "url" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="listusingget_3"></a>
#### 所有菜单列表
```
GET /admin/menu/list
```


##### 说明
所有菜单列表


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**createTime.nanos**  <br>*可选*||integer (int32)|
|**Query**|**createUser**  <br>*可选*||string|
|**Query**|**icon**  <br>*可选*|菜单图标|string|
|**Query**|**id**  <br>*可选*||string|
|**Query**|**list**  <br>*可选*|目录-菜单集合|< object > array(multi)|
|**Query**|**name**  <br>*可选*|菜单名称|string|
|**Query**|**open**  <br>*可选*|z-tree属性|boolean|
|**Query**|**orderNum**  <br>*可选*|排序|integer (int32)|
|**Query**|**parentId**  <br>*可选*|父菜单ID，一级菜单为0|string|
|**Query**|**parentName**  <br>*可选*|父菜单名称|string|
|**Query**|**perms**  <br>*可选*|授权(多个用逗号分隔，如：user:list,user:create)|string|
|**Query**|**type**  <br>*可选*|类型   0：目录   1：菜单   2：按钮|integer (int32)|
|**Query**|**url**  <br>*可选*|菜单URL|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/menu/list
```


###### 请求 query
```
json :
{
  "createTime.nanos" : 0,
  "createUser" : "string",
  "icon" : "string",
  "id" : "string",
  "list" : "object",
  "name" : "string",
  "open" : true,
  "orderNum" : 0,
  "parentId" : "string",
  "parentName" : "string",
  "perms" : "string",
  "type" : 0,
  "url" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="navusingget"></a>
#### 导航菜单
```
GET /admin/menu/nav
```


##### 说明
导航菜单


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/menu/nav
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="listbyroleidusingget"></a>
#### 角色菜单列表
```
GET /admin/menu/role/list
```


##### 说明
角色菜单列表


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**roleId**  <br>*必填*|roleId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/menu/role/list
```


###### 请求 query
```
json :
{
  "roleId" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="selectusingget"></a>
#### 选择菜单(添加、修改菜单)
```
GET /admin/menu/select
```


##### 说明
选择菜单(添加、修改菜单)


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/menu/select
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="infousingget_1"></a>
#### 菜单信息
```
GET /admin/menu/{id}
```


##### 说明
菜单信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|id|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/menu/string
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="deleteusingdelete_1"></a>
#### 删除
```
DELETE /admin/menu/{id}
```


##### 说明
删除


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|id|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/menu/string
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="a2fa4288afb6f7ad0aa7c49bb80f0c63"></a>
### 角色
Role Controller


<a name="saveusingpost_3"></a>
#### 保存角色信息
```
POST /admin/role
```


##### 说明
保存角色信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**roleDto**  <br>*必填*|roleDto|[RoleDTO](#roledto)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/role
```


###### 请求 body
```
json :
{
  "createTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "createUser" : "string",
  "id" : "string",
  "menuIdList" : [ "string" ],
  "menuList" : [ {
    "createTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "createUser" : "string",
    "icon" : "string",
    "id" : "string",
    "list" : [ "object" ],
    "name" : "string",
    "open" : false,
    "orderNum" : 0,
    "parentId" : "string",
    "parentName" : "string",
    "perms" : "string",
    "type" : 0,
    "url" : "string"
  } ],
  "remark" : "string",
  "roleName" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="pageusingget"></a>
#### 分页查询角色接口
```
GET /admin/role
```


##### 说明
条件，分页查询


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**params**  <br>*必填*|params|object|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/role
```


###### 请求 query
```
json :
{
  "params" : "object"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="updateusingput_3"></a>
#### 更新角色
```
PUT /admin/role
```


##### 说明
更新角色


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**roleDto**  <br>*必填*|roleDto|[RoleDTO](#roledto)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/role
```


###### 请求 body
```
json :
{
  "createTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "createUser" : "string",
  "id" : "string",
  "menuIdList" : [ "string" ],
  "menuList" : [ {
    "createTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "createUser" : "string",
    "icon" : "string",
    "id" : "string",
    "list" : [ "object" ],
    "name" : "string",
    "open" : false,
    "orderNum" : 0,
    "parentId" : "string",
    "parentName" : "string",
    "perms" : "string",
    "type" : 0,
    "url" : "string"
  } ],
  "remark" : "string",
  "roleName" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="deleteusingpost_3"></a>
#### 删除角色信息
```
POST /admin/role/delete
```


##### 说明
删除角色信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**ids**  <br>*必填*|ids|< string > array|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/role/delete
```


###### 请求 body
```
json :
[ "string" ]
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="listusingget_5"></a>
#### 角色列表
```
GET /admin/role/list
```


##### 说明
角色列表


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/role/list
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="listbyuserusingget"></a>
#### 用户角色列表
```
GET /admin/role/user/list
```


##### 说明
用户角色列表


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**userId**  <br>*必填*|userId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/role/user/list
```


###### 请求 query
```
json :
{
  "userId" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="infousingget_2"></a>
#### 获取角色信息
```
GET /admin/role/{id}
```


##### 说明
获取角色信息


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|id|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/role/string
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="9a0434a611f0706a82539ba1c1887210"></a>
### 请假
Leave Controller


<a name="saveusingpost_1"></a>
#### 保存
```
POST /leave
```


##### 说明
保存


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**leaveEntity**  <br>*必填*|leaveEntity|[LeaveEntity](#leaveentity)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave
```


###### 请求 body
```
json :
{
  "actHiProcinst" : {
    "businessKey" : "string",
    "deleteReason" : "string",
    "duration" : 0,
    "endActId" : "string",
    "endTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "id" : "string",
    "name" : "string",
    "procDefId" : "string",
    "procInstId" : "string",
    "startActId" : "string",
    "startTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "startUserId" : "string",
    "superProcessInstanceId" : "string",
    "tenantId" : "string"
  },
  "actHiTaskinst" : {
    "assignee" : "string",
    "category" : "string",
    "claimTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "comment" : "string",
    "deleteReason" : "string",
    "description" : "string",
    "dueDate" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "duration" : 0,
    "endTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "executionId" : "string",
    "formKey" : "string",
    "id" : "string",
    "name" : "string",
    "owner" : "string",
    "parentTaskId" : "string",
    "priority" : 0,
    "procDefId" : "string",
    "procInstId" : "string",
    "startTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "taskDefKey" : "string",
    "tenantId" : "string"
  },
  "actRuExecution" : {
    "actId" : "string",
    "businessKey" : "string",
    "cachedEntState" : 0,
    "deadletterJobCount" : 0,
    "evtSubscrCount" : 0,
    "id" : "string",
    "idLinkCount" : 0,
    "isActive" : "string",
    "isConcurrent" : "string",
    "isCountEnabled" : "string",
    "isEventScope" : "string",
    "isMiRoot" : "string",
    "isScope" : "string",
    "jobCount" : 0,
    "lockTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "name" : "string",
    "parentId" : "string",
    "procDefId" : "string",
    "procInstId" : "string",
    "rev" : 0,
    "rootProcInstId" : "string",
    "startTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "startUserId" : "string",
    "superExec" : "string",
    "suspJobCount" : 0,
    "suspensionState" : 0,
    "taskCount" : 0,
    "tenantId" : "string",
    "timerJobCount" : 0,
    "varCount" : 0
  },
  "createTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "days" : 0,
  "endTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "id" : "string",
  "leaveType" : "string",
  "processInstanceId" : "string",
  "reason" : "string",
  "startTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "status" : 0,
  "submitTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "task" : {
    "assignee" : "string",
    "category" : "string",
    "claimTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "createTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "delegation" : "string",
    "description" : "string",
    "dueDate" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "executionId" : "string",
    "formKey" : "string",
    "id" : "string",
    "name" : "string",
    "owner" : "string",
    "parentTaskId" : "string",
    "priority" : 0,
    "procDefId" : "string",
    "procInstId" : "string",
    "rev" : 0,
    "suspensionState" : 0,
    "taskDefKey" : "string",
    "tenantId" : "string"
  },
  "taskList" : [ {
    "assignee" : "string",
    "category" : "string",
    "claimTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "createTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "delegation" : "string",
    "description" : "string",
    "dueDate" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "executionId" : "string",
    "formKey" : "string",
    "id" : "string",
    "name" : "string",
    "owner" : "string",
    "parentTaskId" : "string",
    "priority" : 0,
    "procDefId" : "string",
    "procInstId" : "string",
    "rev" : 0,
    "suspensionState" : 0,
    "taskDefKey" : "string",
    "tenantId" : "string"
  } ],
  "title" : "string",
  "updateTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "userId" : "string",
  "userName" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="updateusingput_1"></a>
#### 更新
```
PUT /leave
```


##### 说明
更新


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**leaveEntity**  <br>*必填*|leaveEntity|[LeaveEntity](#leaveentity)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave
```


###### 请求 body
```
json :
{
  "actHiProcinst" : {
    "businessKey" : "string",
    "deleteReason" : "string",
    "duration" : 0,
    "endActId" : "string",
    "endTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "id" : "string",
    "name" : "string",
    "procDefId" : "string",
    "procInstId" : "string",
    "startActId" : "string",
    "startTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "startUserId" : "string",
    "superProcessInstanceId" : "string",
    "tenantId" : "string"
  },
  "actHiTaskinst" : {
    "assignee" : "string",
    "category" : "string",
    "claimTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "comment" : "string",
    "deleteReason" : "string",
    "description" : "string",
    "dueDate" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "duration" : 0,
    "endTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "executionId" : "string",
    "formKey" : "string",
    "id" : "string",
    "name" : "string",
    "owner" : "string",
    "parentTaskId" : "string",
    "priority" : 0,
    "procDefId" : "string",
    "procInstId" : "string",
    "startTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "taskDefKey" : "string",
    "tenantId" : "string"
  },
  "actRuExecution" : {
    "actId" : "string",
    "businessKey" : "string",
    "cachedEntState" : 0,
    "deadletterJobCount" : 0,
    "evtSubscrCount" : 0,
    "id" : "string",
    "idLinkCount" : 0,
    "isActive" : "string",
    "isConcurrent" : "string",
    "isCountEnabled" : "string",
    "isEventScope" : "string",
    "isMiRoot" : "string",
    "isScope" : "string",
    "jobCount" : 0,
    "lockTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "name" : "string",
    "parentId" : "string",
    "procDefId" : "string",
    "procInstId" : "string",
    "rev" : 0,
    "rootProcInstId" : "string",
    "startTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "startUserId" : "string",
    "superExec" : "string",
    "suspJobCount" : 0,
    "suspensionState" : 0,
    "taskCount" : 0,
    "tenantId" : "string",
    "timerJobCount" : 0,
    "varCount" : 0
  },
  "createTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "days" : 0,
  "endTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "id" : "string",
  "leaveType" : "string",
  "processInstanceId" : "string",
  "reason" : "string",
  "startTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "status" : 0,
  "submitTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "task" : {
    "assignee" : "string",
    "category" : "string",
    "claimTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "createTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "delegation" : "string",
    "description" : "string",
    "dueDate" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "executionId" : "string",
    "formKey" : "string",
    "id" : "string",
    "name" : "string",
    "owner" : "string",
    "parentTaskId" : "string",
    "priority" : 0,
    "procDefId" : "string",
    "procInstId" : "string",
    "rev" : 0,
    "suspensionState" : 0,
    "taskDefKey" : "string",
    "tenantId" : "string"
  },
  "taskList" : [ {
    "assignee" : "string",
    "category" : "string",
    "claimTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "createTime" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "delegation" : "string",
    "description" : "string",
    "dueDate" : {
      "date" : 0,
      "day" : 0,
      "hours" : 0,
      "minutes" : 0,
      "month" : 0,
      "nanos" : 0,
      "seconds" : 0,
      "time" : 0,
      "timezoneOffset" : 0,
      "year" : 0
    },
    "executionId" : "string",
    "formKey" : "string",
    "id" : "string",
    "name" : "string",
    "owner" : "string",
    "parentTaskId" : "string",
    "priority" : 0,
    "procDefId" : "string",
    "procInstId" : "string",
    "rev" : 0,
    "suspensionState" : 0,
    "taskDefKey" : "string",
    "tenantId" : "string"
  } ],
  "title" : "string",
  "updateTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "userId" : "string",
  "userName" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="applyusingpost"></a>
#### 提交申请
```
POST /leave/apply
```


##### 说明
提交申请


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**leaveApplyDTO**  <br>*必填*|leaveApplyDTO|[LeaveApplyDTO](#leaveapplydto)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/apply
```


###### 请求 body
```
json :
{
  "id" : "string",
  "userList" : [ "string" ],
  "users" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="doneusingget"></a>
#### 已办任务
```
GET /leave/done
```


##### 说明
已办任务


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**params**  <br>*必填*|params|object|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/done
```


###### 请求 query
```
json :
{
  "params" : "object"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="finishusingget"></a>
#### 结束的流程
```
GET /leave/finish
```


##### 说明
结束的流程


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**params**  <br>*必填*|params|object|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/finish
```


###### 请求 query
```
json :
{
  "params" : "object"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="historyactivityusingget"></a>
#### 历史活动节点
```
GET /leave/history/activity/{taskId}
```


##### 说明
历史活动节点


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**taskId**  <br>*必填*|taskId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/history/activity/string
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="historytaskusingget"></a>
#### 历史活动任务
```
GET /leave/history/task/{id}
```


##### 说明
历史活动任务


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|id|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/history/task/string
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="listusingget_2"></a>
#### 我的申请
```
GET /leave/mine
```


##### 说明
我的申请


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**params**  <br>*必填*|params|object|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/mine
```


###### 请求 query
```
json :
{
  "params" : "object"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="readresourceusingget"></a>
#### 读取带跟踪的图片
```
GET /leave/process/trace/auto/{id}
```


##### 说明
读取带跟踪的图片


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|id|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/process/trace/auto/string
```


<a name="runningusingget"></a>
#### 运行中的流程
```
GET /leave/running
```


##### 说明
运行中的流程


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**params**  <br>*必填*|params|object|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/running
```


###### 请求 query
```
json :
{
  "params" : "object"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="claimusingget"></a>
#### 签收任务
```
GET /leave/task/claim/{taskId}
```


##### 说明
签收任务


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**taskId**  <br>*必填*|taskId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/task/claim/string
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="completeusingpost"></a>
#### 完成任务
```
POST /leave/task/complete
```


##### 说明
完成任务


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**leaveOperate**  <br>*必填*|leaveOperate|[LeaveOperateDTO](#leaveoperatedto)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/task/complete
```


###### 请求 body
```
json :
{
  "comment" : "string",
  "id" : "string",
  "params" : "object",
  "taskId" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="entrustusingget"></a>
#### 委托任务
```
GET /leave/task/entrust
```


##### 说明
委托任务


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**taskId**  <br>*必填*|taskId|string|
|**Query**|**userId**  <br>*必填*|userId|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/task/entrust
```


###### 请求 query
```
json :
{
  "taskId" : "string",
  "userId" : "string"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="todousingget"></a>
#### 待办任务
```
GET /leave/todo
```


##### 说明
待办任务


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**params**  <br>*必填*|params|object|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/todo
```


###### 请求 query
```
json :
{
  "params" : "object"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="infousingget"></a>
#### 工单详情
```
GET /leave/{id}
```


##### 说明
工单详情


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|id|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/string
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="deleteusingdelete"></a>
#### 删除请假单
```
DELETE /leave/{id}
```


##### 说明
删除请假单


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|ID主键|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### HTTP请求示例

###### 请求 path
```
/leave/string
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="b89551fba1cb502aeb1fd9edb6b18738"></a>
### 调度
Schedule Job Controller


<a name="saveusingpost_4"></a>
#### save
```
POST /admin/schedule
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**scheduleJob**  <br>*必填*|scheduleJob|[SCHEDULE_JOB对象](#d90a08b082c344056003e456e94cdcc3)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/schedule
```


###### 请求 body
```
json :
{
  "beanName" : "string",
  "createTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "cronExpression" : "string",
  "id" : "string",
  "params" : "string",
  "remark" : "string",
  "status" : 0
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="listusingget_6"></a>
#### list
```
GET /admin/schedule
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**params**  <br>*必填*|params|object|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/schedule
```


###### 请求 query
```
json :
{
  "params" : "object"
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="updateusingput_4"></a>
#### update
```
PUT /admin/schedule
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**scheduleJob**  <br>*必填*|scheduleJob|[SCHEDULE_JOB对象](#d90a08b082c344056003e456e94cdcc3)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/schedule
```


###### 请求 body
```
json :
{
  "beanName" : "string",
  "createTime" : {
    "date" : 0,
    "day" : 0,
    "hours" : 0,
    "minutes" : 0,
    "month" : 0,
    "nanos" : 0,
    "seconds" : 0,
    "time" : 0,
    "timezoneOffset" : 0,
    "year" : 0
  },
  "cronExpression" : "string",
  "id" : "string",
  "params" : "string",
  "remark" : "string",
  "status" : 0
}
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="deleteusingpost_4"></a>
#### delete
```
POST /admin/schedule/delete
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**ids**  <br>*必填*|ids|< string > array|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/schedule/delete
```


###### 请求 body
```
json :
[ "string" ]
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="pauseusingpost"></a>
#### pause
```
POST /admin/schedule/pause
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**ids**  <br>*必填*|ids|< string > array|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/schedule/pause
```


###### 请求 body
```
json :
[ "string" ]
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="resumeusingpost"></a>
#### resume
```
POST /admin/schedule/resume
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**ids**  <br>*必填*|ids|< string > array|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/schedule/resume
```


###### 请求 body
```
json :
[ "string" ]
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="runusingpost"></a>
#### run
```
POST /admin/schedule/run
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**ids**  <br>*必填*|ids|< string > array|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/schedule/run
```


###### 请求 body
```
json :
[ "string" ]
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```


<a name="infousingget_3"></a>
#### info
```
GET /admin/schedule/{id}
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|id|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result](#result)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### HTTP请求示例

###### 请求 path
```
/admin/schedule/string
```


##### HTTP响应示例

###### 响应 200
```
json :
{
  "data" : "object",
  "msg" : "成功",
  "success" : true
}
```



