package com.it.cloud.modules.feignTest.config;

import feign.Retryer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.concurrent.TimeUnit;

/**
 * @author 司马缸砸缸了
 * @date 2019/8/2 16:34
 * @description feign自定义配置,也可以使用yml形式，支持全局和局部
 */
@Configuration
public class FeignConfig {
    @Bean
    public Retryer feignRetryer(){
        return new Retryer.Default(100, TimeUnit.SECONDS.toMillis(1),5);
    }
}