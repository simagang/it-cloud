package com.it.cloud.modules.feignTest.controller;

import com.it.cloud.modules.feignTest.service.IFeignService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-12
 */
@Api(value = "feign测试", tags = "feign测试")
@Slf4j
@RestController
@RequestMapping("/feign")
public class FeignController {
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private IFeignService feignService;

    @ApiOperation(value = "测试", notes = "测试")
    @GetMapping(value = "/test")
    public String hi(@RequestParam String name) {
        return hiService( name );
    }

    public String hiService(String name) {
        // restTemplate
        // return restTemplate.getForObject("http://SERVICE-HI/hi?name="+name,String.class);

        return feignService.home("杨洋");
    }


}
