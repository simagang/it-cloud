package com.it.cloud.modules.websocket.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * @author 司马缸砸缸了
 * @date 2019/7/29 17:35
 * @description socket消息
 */

@ApiModel(value = "Socket消息体", description = "Socket消息体")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SocketMessageDTO implements Serializable {

    @ApiModelProperty(value = "socket userId")
    private String userId;

    @ApiModelProperty(value = "消息")
    private String message;
}
