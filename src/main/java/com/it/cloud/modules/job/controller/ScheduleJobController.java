package com.it.cloud.modules.job.controller;

import com.it.cloud.common.annotation.SysLog;
import com.it.cloud.common.base.Result;
import com.it.cloud.common.utils.PageUtils;
import com.it.cloud.common.validator.ValidatorUtils;
import com.it.cloud.modules.job.entity.ScheduleJobEntity;
import com.it.cloud.modules.job.service.IScheduleJobService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-25
 */
@Api(value = "调度控制器", tags = "调度")
@Slf4j
@RestController
@RequestMapping("/admin/schedule")
public class    ScheduleJobController {
    @Autowired
    private IScheduleJobService scheduleJobService;

    @ApiOperation(value = "定时任务列表", notes = "定时任务列表")
    @GetMapping("")
    public Result list(@RequestParam Map<String, Object> params) {
        PageUtils page = scheduleJobService.queryPage(params);

        return Result.ok(page);
    }

    @ApiOperation(value = "定时任务信息", notes = "定时任务信息")
    @GetMapping("/{id}")
    public Result info(@PathVariable("id") String id) {
        ScheduleJobEntity schedule = scheduleJobService.getById(id);

        return Result.ok(schedule);
    }

    @SysLog("保存定时任务")
    @PostMapping("")
    @RequiresPermissions("sys:schedule:save")
    public Result save(@RequestBody ScheduleJobEntity scheduleJob) {
        ValidatorUtils.validateEntity(scheduleJob);
        scheduleJobService.saveJob(scheduleJob);

        return Result.ok();
    }

    @SysLog("修改定时任务")
    @PutMapping("")
    @RequiresPermissions("sys:schedule:update")
    public Result update(@RequestBody ScheduleJobEntity scheduleJob) {
        ValidatorUtils.validateEntity(scheduleJob);
        scheduleJobService.update(scheduleJob);

        return Result.ok();
    }

    @SysLog("删除定时任务")
    @PostMapping("/delete")
    @RequiresPermissions("sys:schedule:delete")
    public Result delete(@RequestBody String[] ids) {
        scheduleJobService.deleteBatch(ids);

        return Result.ok();
    }

    @SysLog("立即执行任务")
    @PostMapping("/run")
    @RequiresPermissions("sys:schedule:run")
    public Result run(@RequestBody String[] ids) {
        scheduleJobService.run(ids);

        return Result.ok();
    }

    @SysLog("暂停定时任务")
    @PostMapping("/pause")
    @RequiresPermissions("sys:schedule:pause")
    public Result pause(@RequestBody String[] ids) {
        scheduleJobService.pause(ids);

        return Result.ok();
    }

    @SysLog("恢复定时任务")
    @PostMapping("/resume")
    @RequiresPermissions("sys:schedule:resume")
    public Result resume(@RequestBody String[] ids) {
        scheduleJobService.resume(ids);

        return Result.ok();
    }
}
