package com.it.cloud.modules.job.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.it.cloud.common.validator.group.AddGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * <p>
 * 
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-25
 */
@ApiModel(value = "SCHEDULE_JOB对象", description = "SCHEDULE_JOB对象")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("IT_SCHEDULE_JOB")
public class ScheduleJobEntity implements Serializable {

    @ApiModelProperty(value = "主键")
    @TableId(value = "ID")
    private String id;

    @ApiModelProperty(value = "bean名称")
    @NotBlank(message="bean名称不能为空", groups = AddGroup.class)
    @TableField("BEAN_NAME")
    private String beanName;

    @ApiModelProperty(value = "参数")
    @TableField("PARAMS")
    private String params;

    @ApiModelProperty(value = "表达式")
    @NotBlank(message="cron表达式不能为空", groups = AddGroup.class)
    @TableField("CRON_EXPRESSION")
    private String cronExpression;

    @ApiModelProperty(value = "状态，0：正常，1：暂停")
    @TableField("STATUS")
    private Integer status;

    @ApiModelProperty(value = "备注")
    @TableField("REMARK")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("CREATE_TIME")
    private Timestamp createTime;


}
