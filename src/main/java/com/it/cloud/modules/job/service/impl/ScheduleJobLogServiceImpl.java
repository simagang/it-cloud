package com.it.cloud.modules.job.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.it.cloud.common.utils.PageUtils;
import com.it.cloud.common.utils.Query;
import com.it.cloud.modules.job.entity.ScheduleJobLogEntity;
import com.it.cloud.modules.job.mapper.ScheduleJobLogMapper;
import com.it.cloud.modules.job.service.IScheduleJobLogService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-25
 */
@Service
public class ScheduleJobLogServiceImpl extends ServiceImpl<ScheduleJobLogMapper, ScheduleJobLogEntity> implements IScheduleJobLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String jobId = (String) params.get("jobId");

        IPage<ScheduleJobLogEntity> page = this.page(new Query<ScheduleJobLogEntity>(params).getPage(),
                Wrappers.<ScheduleJobLogEntity>lambdaQuery()
                        .eq(StringUtils.isNotBlank(jobId), ScheduleJobLogEntity::getJobId, jobId)
                        .orderByDesc(ScheduleJobLogEntity::getCreateTime));

        return new PageUtils(page);
    }

}
