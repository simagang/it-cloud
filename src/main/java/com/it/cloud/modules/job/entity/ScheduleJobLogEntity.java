package com.it.cloud.modules.job.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * <p>
 * 
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-25
 */
@ApiModel(value = "SCHEDULE_JOB_LOG对象", description = "SCHEDULE_JOB_LOG对象")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("IT_SCHEDULE_JOB_LOG")
public class ScheduleJobLogEntity implements Serializable {

    @ApiModelProperty(value = "主键")
    @TableId(value = "ID")
    private String id;

    @ApiModelProperty(value = "任务ID")
    @TableField("JOB_ID")
    private String jobId;

    @ApiModelProperty(value = "bean名称")
    @TableField("BEAN_NAME")
    private String beanName;

    @ApiModelProperty(value = "参数")
    @TableField("PARAMS")
    private String params;

    @ApiModelProperty(value = "任务状态，0：成功 1：失败")
    @TableField("STATUS")
    private Integer status;

    @ApiModelProperty(value = "失败信息")
    @TableField("ERROR")
    private String error;

    @ApiModelProperty(value = "耗时")
    @TableField("TIMES")
    private Integer times;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("CREATE_TIME")
    private Timestamp createTime;


}
