package com.it.cloud.modules.job.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 * <p>
 * 测试定时任务(演示Demo，可删除)
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-25
 */
@Component("testTask")
public class TestTask implements ITask {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void run(String params){

		logger.debug("TestTask定时任务正在执行，参数为：{}", params);
	}
}
