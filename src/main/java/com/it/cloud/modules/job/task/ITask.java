package com.it.cloud.modules.job.task;

/**
 * <p>
 *  定时任务接口，所有定时任务都要实现该接口
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-25
 */
public interface ITask {

    /**
     * 执行定时任务接口
     *
     * @param params   参数，多参数使用JSON数据
     */
    void run(String params);
}