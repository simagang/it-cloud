package com.it.cloud.modules.job.controller;


import com.baomidou.mybatisplus.extension.api.R;
import com.it.cloud.common.base.Result;
import com.it.cloud.common.utils.PageUtils;
import com.it.cloud.modules.job.entity.ScheduleJobLogEntity;
import com.it.cloud.modules.job.service.IScheduleJobLogService;
import com.it.cloud.modules.job.service.IScheduleJobService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-25
 */
@Api(value = "任务日志控制器", tags = "任务日志")
@Slf4j
@RestController
@RequestMapping("/admin/schedule/log")
public class ScheduleJobLogController {

    @Autowired
    private IScheduleJobLogService scheduleJobLogService;

    @ApiOperation(value = "分页查询job日志接口", notes = "条件，分页查询")
    @GetMapping("")
    public Result list(@RequestParam Map<String, Object> params) {
        PageUtils page = scheduleJobLogService.queryPage(params);

        return Result.ok(page);
    }

    @ApiOperation(value = "job日志信息接口", notes = "job日志信息")
    @GetMapping("/{id}")
    public Result info(@PathVariable("id") String id) {
        ScheduleJobLogEntity log = scheduleJobLogService.getById(id);

        return Result.ok(log);
    }
}
