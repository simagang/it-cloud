package com.it.cloud.modules.job.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.it.cloud.common.utils.PageUtils;
import com.it.cloud.modules.job.entity.ScheduleJobEntity;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-25
 */
public interface IScheduleJobService extends IService<ScheduleJobEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存定时任务
     */
    void saveJob(ScheduleJobEntity scheduleJob);

    /**
     * 更新定时任务
     */
    void update(ScheduleJobEntity scheduleJob);

    /**
     * 批量删除定时任务
     */
    void deleteBatch(String[] jobIds);

    /**
     * 批量更新定时任务状态
     */
    void updateBatch(String[] jobIds, int status);

    /**
     * 立即执行
     */
    void run(String[] jobIds);

    /**
     * 暂停运行
     */
    void pause(String[] jobIds);

    /**
     * 恢复运行
     */
    void resume(String[] jobIds);
}
