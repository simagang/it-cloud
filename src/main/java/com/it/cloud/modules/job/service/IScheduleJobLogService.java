package com.it.cloud.modules.job.service;

import com.it.cloud.common.utils.PageUtils;
import com.it.cloud.modules.job.entity.ScheduleJobLogEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-25
 */
public interface IScheduleJobLogService extends IService<ScheduleJobLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}
