package com.it.cloud.modules.job.mapper;

import com.it.cloud.modules.job.entity.ScheduleJobEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-25
 */
public interface ScheduleJobMapper extends BaseMapper<ScheduleJobEntity> {

}
