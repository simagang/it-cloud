package com.it.cloud.modules.system.mapper;

import com.it.cloud.modules.system.entity.SysLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-24
 */
public interface SysLogMapper extends BaseMapper<SysLogEntity> {

}
