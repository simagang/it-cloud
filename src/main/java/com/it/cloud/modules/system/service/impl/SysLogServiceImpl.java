package com.it.cloud.modules.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.it.cloud.common.utils.PageUtils;
import com.it.cloud.common.utils.Query;
import com.it.cloud.modules.system.entity.SysLogEntity;
import com.it.cloud.modules.system.mapper.SysLogMapper;
import com.it.cloud.modules.system.service.ISysLogService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-24
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLogEntity> implements ISysLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String username = (String) params.get("username");
        String operation = (String) params.get("operation");

        IPage<SysLogEntity> page = this.page(new Query<SysLogEntity>(params).getPage(),
                Wrappers.<SysLogEntity>lambdaQuery()
                        .eq(StringUtils.isNotBlank(username), SysLogEntity::getUsername, username)
                        .eq(StringUtils.isNotBlank(operation), SysLogEntity::getOperation, operation)
                        .orderByDesc(SysLogEntity::getCreateTime));

        return new PageUtils(page);
    }
}
