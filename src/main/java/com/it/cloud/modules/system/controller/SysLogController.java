package com.it.cloud.modules.system.controller;


import com.it.cloud.common.base.Result;
import com.it.cloud.common.utils.PageUtils;
import com.it.cloud.modules.system.service.ISysLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-24
 */
@Api(value = "系统日志控制器", tags = "系统日志")
@Slf4j
@RestController
@RequestMapping("/system/log")
public class SysLogController {

    @Autowired
    private ISysLogService sysLogService;

    @ApiOperation(value = "分页查询日志接口", notes = "条件，分页查询")
    @GetMapping("")
    public Result list(@RequestParam Map<String, Object> params) {
        PageUtils page = sysLogService.queryPage(params);

        return Result.ok(page);
    }
}
