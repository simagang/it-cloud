package com.it.cloud.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("IT_SYS_LOG")
public class SysLogEntity implements Serializable {


    @TableId(value = "ID")
    private String id;

    @TableField("USER_ID")
    private String userId;

    @TableField("USERNAME")
    private String username;

    @TableField("OPERATION")
    private String operation;

    @TableField("METHOD")
    private String method;

    @TableField("PARAMS")
    private String params;

    @TableField("TIME")
    private Long time;

    @TableField("IP")
    private String ip;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField("CREATE_TIME")
    private Timestamp createTime;


}
