package com.it.cloud.modules.system.service;

import com.it.cloud.common.utils.PageUtils;
import com.it.cloud.modules.system.entity.SysLogEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-24
 */
public interface ISysLogService extends IService<SysLogEntity> {

    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);
}
