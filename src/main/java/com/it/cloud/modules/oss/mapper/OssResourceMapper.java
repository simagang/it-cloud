package com.it.cloud.modules.oss.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.it.cloud.modules.oss.entity.OssResourceEntiry;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-12
 */
public interface OssResourceMapper extends BaseMapper<OssResourceEntiry> {

}
