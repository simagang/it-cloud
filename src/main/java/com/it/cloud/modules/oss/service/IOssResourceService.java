package com.it.cloud.modules.oss.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.it.cloud.common.utils.PageUtils;
import com.it.cloud.modules.oss.entity.OssResourceEntiry;

import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-12
 */
public interface IOssResourceService extends IService<OssResourceEntiry> {

    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);
}
