package com.it.cloud.modules.oss.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.it.cloud.common.utils.PageUtils;
import com.it.cloud.common.utils.Query;
import com.it.cloud.modules.oss.entity.OssResourceEntiry;
import com.it.cloud.modules.oss.mapper.OssResourceMapper;
import com.it.cloud.modules.oss.service.IOssResourceService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-12
 */
@Service
public class OssResourceServiceImpl extends ServiceImpl<OssResourceMapper, OssResourceEntiry> implements IOssResourceService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = (String) params.get("name");

        IPage<OssResourceEntiry> page = this.page(new Query<OssResourceEntiry>(params).getPage(),
                Wrappers.<OssResourceEntiry>lambdaQuery()
                        .eq(StringUtils.isNotBlank(name), OssResourceEntiry::getName, name)
                        .orderByDesc(OssResourceEntiry::getCreateTime));

        return new PageUtils(page);
    }

}
