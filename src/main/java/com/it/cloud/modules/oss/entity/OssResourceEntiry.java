package com.it.cloud.modules.oss.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * <p>
 *
 * </p>
 *
 * @author 司马缸砸缸了
 * @since 2019-07-26
 */

@ApiModel(value = "OssResource对象", description = "云存储资源表")
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("IT_OSS_RESOURCE")
public class OssResourceEntiry implements Serializable {

    @ApiModelProperty(value = "主键")
    @TableId(value = "ID")
    private String id;

    @ApiModelProperty(value = "名称")
    @TableField("NAME")
    private String name;

    @TableField("URL")
    private String url;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField("CREATE_TIME")
    private Timestamp createTime;


}
