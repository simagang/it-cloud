//package com.it.cloud.config.rabbitmq;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.amqp.core.*;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import javax.annotation.Resource;
//
//
///**
// * RabbitMQ配置
// *
// * @author 司马缸砸缸了
// * @date 2019/7/29 13:44
// * @description
// */
//
//
//@Configuration
//public class RabbitConfig {
//
//    @Resource
//    private RabbitTemplate rabbitTemplate;
//
//
//    /**
//     * 定制化amqp模版      可根据需要定制多个
//     * <p>
//     * <p>
//     * 此处为模版类定义 Jackson消息转换器
//     * ConfirmCallback接口用于实现消息发送到RabbitMQ交换器后接收ack回调   即消息发送到exchange  ack
//     * ReturnCallback接口用于实现消息发送到RabbitMQ 交换器，但无相应队列与交换器绑定时的回调  即消息发送不到任何一个队列中  ack
//     *
//     * @return the amqp template
//     */
//
//    // @Primary
//    @Bean
//    public AmqpTemplate amqpTemplate() {
//        Logger log = LoggerFactory.getLogger(RabbitTemplate.class);
//        // 使用jackson 消息转换器, 传输对象时屏蔽掉，防止二次json转换，多了一个"
//        // rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
//        rabbitTemplate.setEncoding("UTF-8");
//        // 消息发送失败返回到队列中，yml需要配置 publisher-returns: true
//
//        // 当mandatory标志位设置为true时，如果exchange根据自身类型和消息routingKey无法找到一个合适的queue存储消息，
//        // 那么broker会调用basic.return方法将消息返还给生产者;
//        // 当mandatory设置为false时，出现上述情况broker会直接将消息丢弃;
//        // 通俗的讲，mandatory标志告诉broker代理服务器至少将消息route到一个队列中，否则就将消息return给发送者
//        rabbitTemplate.setMandatory(true);
//        rabbitTemplate.setReturnCallback((message, replyCode, replyText, exchange, routingKey) -> {
//            String correlationId = message.getMessageProperties().getCorrelationId();
//            log.debug("消息：{} 发送失败, 应答码：{} 原因：{} 交换机: {}  路由键: {}", correlationId, replyCode, replyText, exchange, routingKey);
//        });
//        // 消息确认，yml需要配置 publisher-confirms: true
//        // 1.消费者确认 2.exchange没有路由到queue
//        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
//            if (ack) {
//                log.debug("消息发送到exchange成功,id: {}", correlationData.getId());
//            } else {
//                log.debug("消息发送到exchange失败,原因: {}", cause);
//            }
//        });
//        return rabbitTemplate;
//    }
//
//    /**
//     * ----------------------------------------------------------------------------Direct exchange test---------------------------------------------------------------------------
//     */
//
//
//    /**
//     * 声明Direct交换机 支持持久化.
//     *
//     * @return the exchange
//     */
//    @Bean("directExchange")
//    public Exchange directExchange() {
//        return ExchangeBuilder.directExchange("DIRECT_EXCHANGE").durable(true).build();
//    }
//
//
//    /**
//     * 声明一个队列 支持持久化.
//     *
//     * @return the queue
//     */
//
//    @Bean("directQueue")
//    public Queue directQueue() {
//        return QueueBuilder.durable("DIRECT_QUEUE").build();
//    }
//
//
//    /**
//     * 通过绑定键 将指定队列绑定到一个指定的交换机 .
//     *
//     * @param queue    the queue
//     * @param exchange the exchange
//     * @return the binding
//     */
//
//    @Bean
//    public Binding directBinding(@Qualifier("directQueue") Queue queue,
//                                 @Qualifier("directExchange") Exchange exchange) {
//        return BindingBuilder.bind(queue).to(exchange).with("DIRECT_ROUTING_KEY").noargs();
//    }
//
//
//    /**
//     * ----------------------------------------------------------------------------Topic exchange test---------------------------------------------------------------------------
//     */
//
//
//    /**
//     * 声明 topic 交换机.
//     *
//     * @return the exchange
//     */
//    @Bean("topicExchange")
//    public TopicExchange topicExchange() {
//        return (TopicExchange) ExchangeBuilder.topicExchange("TOPIC_EXCHANGE").durable(true).build();
//    }
//
//
//    /**
//     * Fanout queue A.
//     *
//     * @return the queue
//     */
//
//    @Bean("topicQueueA")
//    public Queue topicQueueA() {
//        return QueueBuilder.durable("TOPIC_QUEUE_A").build();
//    }
//
//
//    /**
//     * 绑定队列A 到Topic 交换机.
//     *
//     * @param queue         the queue
//     * @param topicExchange the topic exchange
//     * @return the binding
//     */
//
//    @Bean
//    public Binding topicBinding(@Qualifier("topicQueueA") Queue queue,
//                                @Qualifier("topicExchange") TopicExchange topicExchange) {
//        return BindingBuilder.bind(queue).to(topicExchange).with("TOPIC.ROUTE.KEY.*");
//    }
//
//
//    /**
//     * ----------------------------------------------------------------------------Fanout exchange test---------------------------------------------------------------------------
//     */
//
//
//    /**
//     * 声明 fanout 交换机.
//     *
//     * @return the exchange
//     */
//    @Bean("fanoutExchange")
//    public FanoutExchange fanoutExchange() {
//        return (FanoutExchange) ExchangeBuilder.fanoutExchange("FANOUT_EXCHANGE").durable(true).build();
//    }
//
//
//    /**
//     * Fanout queue A.
//     *
//     * @return the queue
//     */
//
//    @Bean("fanoutQueueA")
//    public Queue fanoutQueueA() {
//        return QueueBuilder.durable("FANOUT_QUEUE_A").build();
//    }
//
//
//    /**
//     * Fanout queue B .
//     *
//     * @return the queue
//     */
//
//    @Bean("fanoutQueueB")
//    public Queue fanoutQueueB() {
//        return QueueBuilder.durable("FANOUT_QUEUE_B").build();
//    }
//
//
//    /**
//     * 绑定队列A 到Fanout 交换机.
//     *
//     * @param queue          the queue
//     * @param fanoutExchange the fanout exchange
//     * @return the binding
//     */
//
//    @Bean
//    public Binding bindingA(@Qualifier("fanoutQueueA") Queue queue,
//                            @Qualifier("fanoutExchange") FanoutExchange fanoutExchange) {
//        return BindingBuilder.bind(queue).to(fanoutExchange);
//    }
//
//
//    /**
//     * 绑定队列B 到Fanout 交换机.
//     *
//     * @param queue          the queue
//     * @param fanoutExchange the fanout exchange
//     * @return the binding
//     */
//
//    @Bean
//    public Binding bindingB(@Qualifier("fanoutQueueB") Queue queue,
//                            @Qualifier("fanoutExchange") FanoutExchange fanoutExchange) {
//        return BindingBuilder.bind(queue).to(fanoutExchange);
//    }
//}
