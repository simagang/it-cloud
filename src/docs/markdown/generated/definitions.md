
<a name="definitions"></a>
## 定义

<a name="acthiprocinst"></a>
### ActHiProcinst

|名称|说明|类型|
|---|---|---|
|**businessKey**  <br>*可选*|**样例** : `"string"`|string|
|**deleteReason**  <br>*可选*|**样例** : `"string"`|string|
|**duration**  <br>*可选*|**样例** : `0`|integer (int64)|
|**endActId**  <br>*可选*|**样例** : `"string"`|string|
|**endTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**id**  <br>*可选*|**样例** : `"string"`|string|
|**name**  <br>*可选*|**样例** : `"string"`|string|
|**procDefId**  <br>*可选*|**样例** : `"string"`|string|
|**procInstId**  <br>*可选*|**样例** : `"string"`|string|
|**startActId**  <br>*可选*|**样例** : `"string"`|string|
|**startTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**startUserId**  <br>*可选*|**样例** : `"string"`|string|
|**superProcessInstanceId**  <br>*可选*|**样例** : `"string"`|string|
|**tenantId**  <br>*可选*|**样例** : `"string"`|string|


<a name="acthitaskinst"></a>
### ActHiTaskinst

|名称|说明|类型|
|---|---|---|
|**assignee**  <br>*可选*|**样例** : `"string"`|string|
|**category**  <br>*可选*|**样例** : `"string"`|string|
|**claimTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**comment**  <br>*可选*|审批意见  <br>**样例** : `"string"`|string|
|**deleteReason**  <br>*可选*|**样例** : `"string"`|string|
|**description**  <br>*可选*|**样例** : `"string"`|string|
|**dueDate**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**duration**  <br>*可选*|**样例** : `0`|integer (int64)|
|**endTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**executionId**  <br>*可选*|**样例** : `"string"`|string|
|**formKey**  <br>*可选*|**样例** : `"string"`|string|
|**id**  <br>*可选*|**样例** : `"string"`|string|
|**name**  <br>*可选*|**样例** : `"string"`|string|
|**owner**  <br>*可选*|**样例** : `"string"`|string|
|**parentTaskId**  <br>*可选*|**样例** : `"string"`|string|
|**priority**  <br>*可选*|**样例** : `0`|integer (int32)|
|**procDefId**  <br>*可选*|**样例** : `"string"`|string|
|**procInstId**  <br>*可选*|**样例** : `"string"`|string|
|**startTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**taskDefKey**  <br>*可选*|**样例** : `"string"`|string|
|**tenantId**  <br>*可选*|**样例** : `"string"`|string|


<a name="actremodelentity"></a>
### ActReModelEntity

|名称|说明|类型|
|---|---|---|
|**category**  <br>*可选*|类型  <br>**样例** : `"string"`|string|
|**createTime**  <br>*可选*|创建时间  <br>**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**deploymentId**  <br>*可选*|部署ID  <br>**样例** : `"string"`|string|
|**description**  <br>*可选*|**样例** : `"string"`|string|
|**editorSourceExtraValueId**  <br>*可选*|编辑源额外值ID，是 ACT_GE_BYTEARRAY 表中的ID_值  <br>**样例** : `"string"`|string|
|**editorSourceValueId**  <br>*可选*|编辑源值ID，是 ACT_GE_BYTEARRAY 表中的ID_值  <br>**样例** : `"string"`|string|
|**id**  <br>*可选*|**样例** : `"string"`|string|
|**key**  <br>*可选*|模型的关键字  <br>**样例** : `"string"`|string|
|**lastUpdateTime**  <br>*可选*|最后修改时间  <br>**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**metaInfo**  <br>*可选*|数据元信息，json格式  <br>**样例** : `"string"`|string|
|**name**  <br>*可选*|模型的名称  <br>**样例** : `"string"`|string|
|**rev**  <br>*可选*|乐观锁版本号  <br>**样例** : `0`|integer (int32)|
|**tenantId**  <br>*可选*|**样例** : `"string"`|string|
|**version**  <br>*可选*|版本，从1开始  <br>**样例** : `0`|integer (int32)|


<a name="actruexecution"></a>
### ActRuExecution

|名称|说明|类型|
|---|---|---|
|**actId**  <br>*可选*|**样例** : `"string"`|string|
|**businessKey**  <br>*可选*|**样例** : `"string"`|string|
|**cachedEntState**  <br>*可选*|**样例** : `0`|integer (int32)|
|**deadletterJobCount**  <br>*可选*|**样例** : `0`|integer (int32)|
|**evtSubscrCount**  <br>*可选*|**样例** : `0`|integer (int32)|
|**id**  <br>*可选*|**样例** : `"string"`|string|
|**idLinkCount**  <br>*可选*|**样例** : `0`|integer (int32)|
|**isActive**  <br>*可选*|**样例** : `"string"`|string|
|**isConcurrent**  <br>*可选*|**样例** : `"string"`|string|
|**isCountEnabled**  <br>*可选*|**样例** : `"string"`|string|
|**isEventScope**  <br>*可选*|**样例** : `"string"`|string|
|**isMiRoot**  <br>*可选*|**样例** : `"string"`|string|
|**isScope**  <br>*可选*|**样例** : `"string"`|string|
|**jobCount**  <br>*可选*|**样例** : `0`|integer (int32)|
|**lockTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**name**  <br>*可选*|**样例** : `"string"`|string|
|**parentId**  <br>*可选*|**样例** : `"string"`|string|
|**procDefId**  <br>*可选*|**样例** : `"string"`|string|
|**procInstId**  <br>*可选*|**样例** : `"string"`|string|
|**rev**  <br>*可选*|**样例** : `0`|integer (int32)|
|**rootProcInstId**  <br>*可选*|**样例** : `"string"`|string|
|**startTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**startUserId**  <br>*可选*|**样例** : `"string"`|string|
|**superExec**  <br>*可选*|**样例** : `"string"`|string|
|**suspJobCount**  <br>*可选*|**样例** : `0`|integer (int32)|
|**suspensionState**  <br>*可选*|**样例** : `0`|integer (int32)|
|**taskCount**  <br>*可选*|**样例** : `0`|integer (int32)|
|**tenantId**  <br>*可选*|**样例** : `"string"`|string|
|**timerJobCount**  <br>*可选*|**样例** : `0`|integer (int32)|
|**varCount**  <br>*可选*|**样例** : `0`|integer (int32)|


<a name="actrutaskentity"></a>
### ActRuTaskEntity

|名称|说明|类型|
|---|---|---|
|**assignee**  <br>*可选*|**样例** : `"string"`|string|
|**category**  <br>*可选*|**样例** : `"string"`|string|
|**claimTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**createTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**delegation**  <br>*可选*|**样例** : `"string"`|string|
|**description**  <br>*可选*|**样例** : `"string"`|string|
|**dueDate**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**executionId**  <br>*可选*|**样例** : `"string"`|string|
|**formKey**  <br>*可选*|**样例** : `"string"`|string|
|**id**  <br>*可选*|**样例** : `"string"`|string|
|**name**  <br>*可选*|**样例** : `"string"`|string|
|**owner**  <br>*可选*|**样例** : `"string"`|string|
|**parentTaskId**  <br>*可选*|**样例** : `"string"`|string|
|**priority**  <br>*可选*|**样例** : `0`|integer (int32)|
|**procDefId**  <br>*可选*|**样例** : `"string"`|string|
|**procInstId**  <br>*可选*|**样例** : `"string"`|string|
|**rev**  <br>*可选*|**样例** : `0`|integer (int32)|
|**suspensionState**  <br>*可选*|**样例** : `0`|integer (int32)|
|**taskDefKey**  <br>*可选*|**样例** : `"string"`|string|
|**tenantId**  <br>*可选*|**样例** : `"string"`|string|


<a name="leaveapplydto"></a>
### LeaveApplyDTO

|名称|说明|类型|
|---|---|---|
|**id**  <br>*必填*|请假单Id  <br>**样例** : `"string"`|string|
|**userList**  <br>*必填*|申批人ID集合  <br>**样例** : `[ "string" ]`|< string > array|
|**users**  <br>*必填*|申批人  <br>**样例** : `"string"`|string|


<a name="leaveentity"></a>
### LeaveEntity

|名称|说明|类型|
|---|---|---|
|**actHiProcinst**  <br>*可选*|历史流程实例  <br>**样例** : `"[acthiprocinst](#acthiprocinst)"`|[ActHiProcinst](#acthiprocinst)|
|**actHiTaskinst**  <br>*可选*|历史流程任务  <br>**样例** : `"[acthitaskinst](#acthitaskinst)"`|[ActHiTaskinst](#acthitaskinst)|
|**actRuExecution**  <br>*可选*|流程实例  <br>**样例** : `"[actruexecution](#actruexecution)"`|[ActRuExecution](#actruexecution)|
|**createTime**  <br>*可选*|创建时间  <br>**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**days**  <br>*可选*|**样例** : `0`|integer (int32)|
|**endTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**id**  <br>*可选*|**样例** : `"string"`|string|
|**leaveType**  <br>*可选*|**样例** : `"string"`|string|
|**processInstanceId**  <br>*可选*|**样例** : `"string"`|string|
|**reason**  <br>*可选*|**样例** : `"string"`|string|
|**startTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**status**  <br>*可选*|**样例** : `0`|integer (int32)|
|**submitTime**  <br>*可选*|提交时间  <br>**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**task**  <br>*可选*|流程任务  <br>**样例** : `"[actrutaskentity](#actrutaskentity)"`|[ActRuTaskEntity](#actrutaskentity)|
|**taskList**  <br>*可选*|流程任务集合  <br>**样例** : `[ "[actrutaskentity](#actrutaskentity)" ]`|< [ActRuTaskEntity](#actrutaskentity) > array|
|**title**  <br>*可选*|**样例** : `"string"`|string|
|**updateTime**  <br>*可选*|更新时间  <br>**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**userId**  <br>*可选*|**样例** : `"string"`|string|
|**userName**  <br>*可选*|**样例** : `"string"`|string|


<a name="leaveoperatedto"></a>
### LeaveOperateDTO

|名称|说明|类型|
|---|---|---|
|**comment**  <br>*可选*|审批意见  <br>**样例** : `"string"`|string|
|**id**  <br>*必填*|工单id  <br>**样例** : `"string"`|string|
|**params**  <br>*可选*|流程参数  <br>**样例** : `"object"`|object|
|**taskId**  <br>*必填*|任务id  <br>**样例** : `"string"`|string|


<a name="menuentity"></a>
### MenuEntity

|名称|说明|类型|
|---|---|---|
|**createTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**createUser**  <br>*可选*|**样例** : `"string"`|string|
|**icon**  <br>*可选*|菜单图标  <br>**样例** : `"string"`|string|
|**id**  <br>*可选*|**样例** : `"string"`|string|
|**list**  <br>*可选*|目录-菜单集合  <br>**样例** : `[ "object" ]`|< object > array|
|**name**  <br>*可选*|菜单名称  <br>**样例** : `"string"`|string|
|**open**  <br>*可选*|z-tree属性  <br>**样例** : `false`|boolean|
|**orderNum**  <br>*可选*|排序  <br>**样例** : `0`|integer (int32)|
|**parentId**  <br>*可选*|父菜单ID，一级菜单为0  <br>**样例** : `"string"`|string|
|**parentName**  <br>*可选*|父菜单名称  <br>**样例** : `"string"`|string|
|**perms**  <br>*可选*|授权(多个用逗号分隔，如：user:list,user:create)  <br>**样例** : `"string"`|string|
|**type**  <br>*可选*|类型   0：目录   1：菜单   2：按钮  <br>**样例** : `0`|integer (int32)|
|**url**  <br>*可选*|菜单URL  <br>**样例** : `"string"`|string|


<a name="modelandview"></a>
### ModelAndView

|名称|说明|类型|
|---|---|---|
|**empty**  <br>*可选*|**样例** : `true`|boolean|
|**model**  <br>*可选*|**样例** : `"object"`|object|
|**modelMap**  <br>*可选*|**样例** : `{<br>  "string" : "object"<br>}`|< string, object > map|
|**reference**  <br>*可选*|**样例** : `true`|boolean|
|**status**  <br>*可选*|**样例** : `"string"`|enum (100 CONTINUE, 101 SWITCHING_PROTOCOLS, 102 PROCESSING, 103 CHECKPOINT, 200 OK, 201 CREATED, 202 ACCEPTED, 203 NON_AUTHORITATIVE_INFORMATION, 204 NO_CONTENT, 205 RESET_CONTENT, 206 PARTIAL_CONTENT, 207 MULTI_STATUS, 208 ALREADY_REPORTED, 226 IM_USED, 300 MULTIPLE_CHOICES, 301 MOVED_PERMANENTLY, 302 FOUND, 302 MOVED_TEMPORARILY, 303 SEE_OTHER, 304 NOT_MODIFIED, 305 USE_PROXY, 307 TEMPORARY_REDIRECT, 308 PERMANENT_REDIRECT, 400 BAD_REQUEST, 401 UNAUTHORIZED, 402 PAYMENT_REQUIRED, 403 FORBIDDEN, 404 NOT_FOUND, 405 METHOD_NOT_ALLOWED, 406 NOT_ACCEPTABLE, 407 PROXY_AUTHENTICATION_REQUIRED, 408 REQUEST_TIMEOUT, 409 CONFLICT, 410 GONE, 411 LENGTH_REQUIRED, 412 PRECONDITION_FAILED, 413 PAYLOAD_TOO_LARGE, 413 REQUEST_ENTITY_TOO_LARGE, 414 URI_TOO_LONG, 414 REQUEST_URI_TOO_LONG, 415 UNSUPPORTED_MEDIA_TYPE, 416 REQUESTED_RANGE_NOT_SATISFIABLE, 417 EXPECTATION_FAILED, 418 I_AM_A_TEAPOT, 419 INSUFFICIENT_SPACE_ON_RESOURCE, 420 METHOD_FAILURE, 421 DESTINATION_LOCKED, 422 UNPROCESSABLE_ENTITY, 423 LOCKED, 424 FAILED_DEPENDENCY, 426 UPGRADE_REQUIRED, 428 PRECONDITION_REQUIRED, 429 TOO_MANY_REQUESTS, 431 REQUEST_HEADER_FIELDS_TOO_LARGE, 451 UNAVAILABLE_FOR_LEGAL_REASONS, 500 INTERNAL_SERVER_ERROR, 501 NOT_IMPLEMENTED, 502 BAD_GATEWAY, 503 SERVICE_UNAVAILABLE, 504 GATEWAY_TIMEOUT, 505 HTTP_VERSION_NOT_SUPPORTED, 506 VARIANT_ALSO_NEGOTIATES, 507 INSUFFICIENT_STORAGE, 508 LOOP_DETECTED, 509 BANDWIDTH_LIMIT_EXCEEDED, 510 NOT_EXTENDED, 511 NETWORK_AUTHENTICATION_REQUIRED)|
|**view**  <br>*可选*|**样例** : `"[view](#view)"`|[View](#view)|
|**viewName**  <br>*可选*|**样例** : `"string"`|string|


<a name="objectnode"></a>
### ObjectNode

|名称|说明|类型|
|---|---|---|
|**array**  <br>*可选*|**样例** : `true`|boolean|
|**bigDecimal**  <br>*可选*|**样例** : `true`|boolean|
|**bigInteger**  <br>*可选*|**样例** : `true`|boolean|
|**binary**  <br>*可选*|**样例** : `true`|boolean|
|**boolean**  <br>*可选*|**样例** : `true`|boolean|
|**containerNode**  <br>*可选*|**样例** : `true`|boolean|
|**double**  <br>*可选*|**样例** : `true`|boolean|
|**float**  <br>*可选*|**样例** : `true`|boolean|
|**floatingPointNumber**  <br>*可选*|**样例** : `true`|boolean|
|**int**  <br>*可选*|**样例** : `true`|boolean|
|**integralNumber**  <br>*可选*|**样例** : `true`|boolean|
|**long**  <br>*可选*|**样例** : `true`|boolean|
|**missingNode**  <br>*可选*|**样例** : `true`|boolean|
|**nodeType**  <br>*可选*|**样例** : `"string"`|enum (ARRAY, BINARY, BOOLEAN, MISSING, NULL, NUMBER, OBJECT, POJO, STRING)|
|**null**  <br>*可选*|**样例** : `true`|boolean|
|**number**  <br>*可选*|**样例** : `true`|boolean|
|**object**  <br>*可选*|**样例** : `true`|boolean|
|**pojo**  <br>*可选*|**样例** : `true`|boolean|
|**short**  <br>*可选*|**样例** : `true`|boolean|
|**textual**  <br>*可选*|**样例** : `true`|boolean|
|**valueNode**  <br>*可选*|**样例** : `true`|boolean|


<a name="result"></a>
### Result
API接口的返回对象


|名称|说明|类型|
|---|---|---|
|**data**  <br>*可选*|返回的数据  <br>**样例** : `"object"`|object|
|**msg**  <br>*可选*|返回的详细说明  <br>**样例** : `"成功"`|string|
|**success**  <br>*必填*|是否成功  <br>**样例** : `true`|boolean|


<a name="roledto"></a>
### RoleDTO

|名称|说明|类型|
|---|---|---|
|**createTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**createUser**  <br>*可选*|**样例** : `"string"`|string|
|**id**  <br>*可选*|**样例** : `"string"`|string|
|**menuIdList**  <br>*可选*|菜单ID集合  <br>**样例** : `[ "string" ]`|< string > array|
|**menuList**  <br>*可选*|**样例** : `[ "[menuentity](#menuentity)" ]`|< [MenuEntity](#menuentity) > array|
|**remark**  <br>*可选*|**样例** : `"string"`|string|
|**roleName**  <br>*可选*|**样例** : `"string"`|string|


<a name="roleentity"></a>
### RoleEntity

|名称|说明|类型|
|---|---|---|
|**createTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**createUser**  <br>*可选*|**样例** : `"string"`|string|
|**id**  <br>*可选*|**样例** : `"string"`|string|
|**menuList**  <br>*可选*|**样例** : `[ "[menuentity](#menuentity)" ]`|< [MenuEntity](#menuentity) > array|
|**remark**  <br>*可选*|**样例** : `"string"`|string|
|**roleName**  <br>*可选*|**样例** : `"string"`|string|


<a name="rolemenudto"></a>
### RoleMenuDTO

|名称|说明|类型|
|---|---|---|
|**menuList**  <br>*必填*|菜单ID集合  <br>**样例** : `[ "string" ]`|< string > array|
|**roleId**  <br>*必填*|角色Id  <br>**样例** : `"string"`|string|


<a name="d90a08b082c344056003e456e94cdcc3"></a>
### SCHEDULE_JOB对象
SCHEDULE_JOB对象


|名称|说明|类型|
|---|---|---|
|**beanName**  <br>*可选*|bean名称  <br>**样例** : `"string"`|string|
|**createTime**  <br>*可选*|创建时间  <br>**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**cronExpression**  <br>*可选*|表达式  <br>**样例** : `"string"`|string|
|**id**  <br>*可选*|主键  <br>**样例** : `"string"`|string|
|**params**  <br>*可选*|参数  <br>**样例** : `"string"`|string|
|**remark**  <br>*可选*|备注  <br>**样例** : `"string"`|string|
|**status**  <br>*可选*|状态，0：正常，1：暂停  <br>**样例** : `0`|integer (int32)|


<a name="timestamp"></a>
### Timestamp

|名称|说明|类型|
|---|---|---|
|**date**  <br>*可选*|**样例** : `0`|integer (int32)|
|**day**  <br>*可选*|**样例** : `0`|integer (int32)|
|**hours**  <br>*可选*|**样例** : `0`|integer (int32)|
|**minutes**  <br>*可选*|**样例** : `0`|integer (int32)|
|**month**  <br>*可选*|**样例** : `0`|integer (int32)|
|**nanos**  <br>*可选*|**样例** : `0`|integer (int32)|
|**seconds**  <br>*可选*|**样例** : `0`|integer (int32)|
|**time**  <br>*可选*|**样例** : `0`|integer (int64)|
|**timezoneOffset**  <br>*可选*|**样例** : `0`|integer (int32)|
|**year**  <br>*可选*|**样例** : `0`|integer (int32)|


<a name="userentity"></a>
### UserEntity

|名称|说明|类型|
|---|---|---|
|**age**  <br>*可选*|**样例** : `0`|integer (int32)|
|**createTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**createUser**  <br>*可选*|**样例** : `"string"`|string|
|**deleteStatus**  <br>*可选*|**样例** : `0`|integer (int32)|
|**email**  <br>*可选*|**样例** : `"string"`|string|
|**id**  <br>*可选*|**样例** : `"string"`|string|
|**mobile**  <br>*可选*|**样例** : `"string"`|string|
|**password**  <br>*可选*|**样例** : `"string"`|string|
|**roleList**  <br>*可选*|**样例** : `[ "[roleentity](#roleentity)" ]`|< [RoleEntity](#roleentity) > array|
|**salt**  <br>*可选*|**样例** : `"string"`|string|
|**sex**  <br>*可选*|**样例** : `"string"`|string|
|**status**  <br>*可选*|**样例** : `0`|integer (int32)|
|**updateTime**  <br>*可选*|**样例** : `"[timestamp](#timestamp)"`|[Timestamp](#timestamp)|
|**updateUser**  <br>*可选*|**样例** : `"string"`|string|
|**username**  <br>*可选*|**样例** : `"string"`|string|


<a name="userroledto"></a>
### UserRoleDTO

|名称|说明|类型|
|---|---|---|
|**roleList**  <br>*必填*|角色ID集合  <br>**样例** : `[ "string" ]`|< string > array|
|**userId**  <br>*必填*|用户Id  <br>**样例** : `"string"`|string|


<a name="view"></a>
### View

|名称|说明|类型|
|---|---|---|
|**contentType**  <br>*可选*|**样例** : `"string"`|string|


<a name="fea127974cda404395799413a0680320"></a>
### 登录表单对象
登录表单对象


|名称|说明|类型|
|---|---|---|
|**captcha**  <br>*可选*|验证码  <br>**样例** : `"string"`|string|
|**password**  <br>*可选*|密码  <br>**样例** : `"string"`|string|
|**username**  <br>*可选*|用户名  <br>**样例** : `"string"`|string|
|**uuid**  <br>*可选*|生成验证码的UUID  <br>**样例** : `"string"`|string|



