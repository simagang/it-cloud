
[[_definitions]]
== 定义

[[_acthiprocinst]]
=== ActHiProcinst

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**businessKey** +
__可选__|**样例** : `"string"`|string
|**deleteReason** +
__可选__|**样例** : `"string"`|string
|**duration** +
__可选__|**样例** : `0`|integer (int64)
|**endActId** +
__可选__|**样例** : `"string"`|string
|**endTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**id** +
__可选__|**样例** : `"string"`|string
|**name** +
__可选__|**样例** : `"string"`|string
|**procDefId** +
__可选__|**样例** : `"string"`|string
|**procInstId** +
__可选__|**样例** : `"string"`|string
|**startActId** +
__可选__|**样例** : `"string"`|string
|**startTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**startUserId** +
__可选__|**样例** : `"string"`|string
|**superProcessInstanceId** +
__可选__|**样例** : `"string"`|string
|**tenantId** +
__可选__|**样例** : `"string"`|string
|===


[[_acthitaskinst]]
=== ActHiTaskinst

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**assignee** +
__可选__|**样例** : `"string"`|string
|**category** +
__可选__|**样例** : `"string"`|string
|**claimTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**comment** +
__可选__|审批意见 +
**样例** : `"string"`|string
|**deleteReason** +
__可选__|**样例** : `"string"`|string
|**description** +
__可选__|**样例** : `"string"`|string
|**dueDate** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**duration** +
__可选__|**样例** : `0`|integer (int64)
|**endTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**executionId** +
__可选__|**样例** : `"string"`|string
|**formKey** +
__可选__|**样例** : `"string"`|string
|**id** +
__可选__|**样例** : `"string"`|string
|**name** +
__可选__|**样例** : `"string"`|string
|**owner** +
__可选__|**样例** : `"string"`|string
|**parentTaskId** +
__可选__|**样例** : `"string"`|string
|**priority** +
__可选__|**样例** : `0`|integer (int32)
|**procDefId** +
__可选__|**样例** : `"string"`|string
|**procInstId** +
__可选__|**样例** : `"string"`|string
|**startTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**taskDefKey** +
__可选__|**样例** : `"string"`|string
|**tenantId** +
__可选__|**样例** : `"string"`|string
|===


[[_actremodelentity]]
=== ActReModelEntity

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**category** +
__可选__|类型 +
**样例** : `"string"`|string
|**createTime** +
__可选__|创建时间 +
**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**deploymentId** +
__可选__|部署ID +
**样例** : `"string"`|string
|**description** +
__可选__|**样例** : `"string"`|string
|**editorSourceExtraValueId** +
__可选__|编辑源额外值ID，是 ACT_GE_BYTEARRAY 表中的ID_值 +
**样例** : `"string"`|string
|**editorSourceValueId** +
__可选__|编辑源值ID，是 ACT_GE_BYTEARRAY 表中的ID_值 +
**样例** : `"string"`|string
|**id** +
__可选__|**样例** : `"string"`|string
|**key** +
__可选__|模型的关键字 +
**样例** : `"string"`|string
|**lastUpdateTime** +
__可选__|最后修改时间 +
**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**metaInfo** +
__可选__|数据元信息，json格式 +
**样例** : `"string"`|string
|**name** +
__可选__|模型的名称 +
**样例** : `"string"`|string
|**rev** +
__可选__|乐观锁版本号 +
**样例** : `0`|integer (int32)
|**tenantId** +
__可选__|**样例** : `"string"`|string
|**version** +
__可选__|版本，从1开始 +
**样例** : `0`|integer (int32)
|===


[[_actruexecution]]
=== ActRuExecution

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**actId** +
__可选__|**样例** : `"string"`|string
|**businessKey** +
__可选__|**样例** : `"string"`|string
|**cachedEntState** +
__可选__|**样例** : `0`|integer (int32)
|**deadletterJobCount** +
__可选__|**样例** : `0`|integer (int32)
|**evtSubscrCount** +
__可选__|**样例** : `0`|integer (int32)
|**id** +
__可选__|**样例** : `"string"`|string
|**idLinkCount** +
__可选__|**样例** : `0`|integer (int32)
|**isActive** +
__可选__|**样例** : `"string"`|string
|**isConcurrent** +
__可选__|**样例** : `"string"`|string
|**isCountEnabled** +
__可选__|**样例** : `"string"`|string
|**isEventScope** +
__可选__|**样例** : `"string"`|string
|**isMiRoot** +
__可选__|**样例** : `"string"`|string
|**isScope** +
__可选__|**样例** : `"string"`|string
|**jobCount** +
__可选__|**样例** : `0`|integer (int32)
|**lockTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**name** +
__可选__|**样例** : `"string"`|string
|**parentId** +
__可选__|**样例** : `"string"`|string
|**procDefId** +
__可选__|**样例** : `"string"`|string
|**procInstId** +
__可选__|**样例** : `"string"`|string
|**rev** +
__可选__|**样例** : `0`|integer (int32)
|**rootProcInstId** +
__可选__|**样例** : `"string"`|string
|**startTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**startUserId** +
__可选__|**样例** : `"string"`|string
|**superExec** +
__可选__|**样例** : `"string"`|string
|**suspJobCount** +
__可选__|**样例** : `0`|integer (int32)
|**suspensionState** +
__可选__|**样例** : `0`|integer (int32)
|**taskCount** +
__可选__|**样例** : `0`|integer (int32)
|**tenantId** +
__可选__|**样例** : `"string"`|string
|**timerJobCount** +
__可选__|**样例** : `0`|integer (int32)
|**varCount** +
__可选__|**样例** : `0`|integer (int32)
|===


[[_actrutaskentity]]
=== ActRuTaskEntity

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**assignee** +
__可选__|**样例** : `"string"`|string
|**category** +
__可选__|**样例** : `"string"`|string
|**claimTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**createTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**delegation** +
__可选__|**样例** : `"string"`|string
|**description** +
__可选__|**样例** : `"string"`|string
|**dueDate** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**executionId** +
__可选__|**样例** : `"string"`|string
|**formKey** +
__可选__|**样例** : `"string"`|string
|**id** +
__可选__|**样例** : `"string"`|string
|**name** +
__可选__|**样例** : `"string"`|string
|**owner** +
__可选__|**样例** : `"string"`|string
|**parentTaskId** +
__可选__|**样例** : `"string"`|string
|**priority** +
__可选__|**样例** : `0`|integer (int32)
|**procDefId** +
__可选__|**样例** : `"string"`|string
|**procInstId** +
__可选__|**样例** : `"string"`|string
|**rev** +
__可选__|**样例** : `0`|integer (int32)
|**suspensionState** +
__可选__|**样例** : `0`|integer (int32)
|**taskDefKey** +
__可选__|**样例** : `"string"`|string
|**tenantId** +
__可选__|**样例** : `"string"`|string
|===


[[_leaveapplydto]]
=== LeaveApplyDTO

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**id** +
__必填__|请假单Id +
**样例** : `"string"`|string
|**userList** +
__必填__|申批人ID集合 +
**样例** : `[ "string" ]`|< string > array
|**users** +
__必填__|申批人 +
**样例** : `"string"`|string
|===


[[_leaveentity]]
=== LeaveEntity

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**actHiProcinst** +
__可选__|历史流程实例 +
**样例** : `"<<_acthiprocinst>>"`|<<_acthiprocinst,ActHiProcinst>>
|**actHiTaskinst** +
__可选__|历史流程任务 +
**样例** : `"<<_acthitaskinst>>"`|<<_acthitaskinst,ActHiTaskinst>>
|**actRuExecution** +
__可选__|流程实例 +
**样例** : `"<<_actruexecution>>"`|<<_actruexecution,ActRuExecution>>
|**createTime** +
__可选__|创建时间 +
**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**days** +
__可选__|**样例** : `0`|integer (int32)
|**endTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**id** +
__可选__|**样例** : `"string"`|string
|**leaveType** +
__可选__|**样例** : `"string"`|string
|**processInstanceId** +
__可选__|**样例** : `"string"`|string
|**reason** +
__可选__|**样例** : `"string"`|string
|**startTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**status** +
__可选__|**样例** : `0`|integer (int32)
|**submitTime** +
__可选__|提交时间 +
**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**task** +
__可选__|流程任务 +
**样例** : `"<<_actrutaskentity>>"`|<<_actrutaskentity,ActRuTaskEntity>>
|**taskList** +
__可选__|流程任务集合 +
**样例** : `[ "<<_actrutaskentity>>" ]`|< <<_actrutaskentity,ActRuTaskEntity>> > array
|**title** +
__可选__|**样例** : `"string"`|string
|**updateTime** +
__可选__|更新时间 +
**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**userId** +
__可选__|**样例** : `"string"`|string
|**userName** +
__可选__|**样例** : `"string"`|string
|===


[[_leaveoperatedto]]
=== LeaveOperateDTO

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**comment** +
__可选__|审批意见 +
**样例** : `"string"`|string
|**id** +
__必填__|工单id +
**样例** : `"string"`|string
|**params** +
__可选__|流程参数 +
**样例** : `"object"`|object
|**taskId** +
__必填__|任务id +
**样例** : `"string"`|string
|===


[[_menuentity]]
=== MenuEntity

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**createTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**createUser** +
__可选__|**样例** : `"string"`|string
|**icon** +
__可选__|菜单图标 +
**样例** : `"string"`|string
|**id** +
__可选__|**样例** : `"string"`|string
|**list** +
__可选__|目录-菜单集合 +
**样例** : `[ "object" ]`|< object > array
|**name** +
__可选__|菜单名称 +
**样例** : `"string"`|string
|**open** +
__可选__|z-tree属性 +
**样例** : `false`|boolean
|**orderNum** +
__可选__|排序 +
**样例** : `0`|integer (int32)
|**parentId** +
__可选__|父菜单ID，一级菜单为0 +
**样例** : `"string"`|string
|**parentName** +
__可选__|父菜单名称 +
**样例** : `"string"`|string
|**perms** +
__可选__|授权(多个用逗号分隔，如：user:list,user:create) +
**样例** : `"string"`|string
|**type** +
__可选__|类型 0：目录 1：菜单 2：按钮 +
**样例** : `0`|integer (int32)
|**url** +
__可选__|菜单URL +
**样例** : `"string"`|string
|===


[[_modelandview]]
=== ModelAndView

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**empty** +
__可选__|**样例** : `true`|boolean
|**model** +
__可选__|**样例** : `"object"`|object
|**modelMap** +
__可选__|**样例** : `{
  "string" : "object"
}`|< string, object > map
|**reference** +
__可选__|**样例** : `true`|boolean
|**status** +
__可选__|**样例** : `"string"`|enum (100 CONTINUE, 101 SWITCHING_PROTOCOLS, 102 PROCESSING, 103 CHECKPOINT, 200 OK, 201 CREATED, 202 ACCEPTED, 203 NON_AUTHORITATIVE_INFORMATION, 204 NO_CONTENT, 205 RESET_CONTENT, 206 PARTIAL_CONTENT, 207 MULTI_STATUS, 208 ALREADY_REPORTED, 226 IM_USED, 300 MULTIPLE_CHOICES, 301 MOVED_PERMANENTLY, 302 FOUND, 302 MOVED_TEMPORARILY, 303 SEE_OTHER, 304 NOT_MODIFIED, 305 USE_PROXY, 307 TEMPORARY_REDIRECT, 308 PERMANENT_REDIRECT, 400 BAD_REQUEST, 401 UNAUTHORIZED, 402 PAYMENT_REQUIRED, 403 FORBIDDEN, 404 NOT_FOUND, 405 METHOD_NOT_ALLOWED, 406 NOT_ACCEPTABLE, 407 PROXY_AUTHENTICATION_REQUIRED, 408 REQUEST_TIMEOUT, 409 CONFLICT, 410 GONE, 411 LENGTH_REQUIRED, 412 PRECONDITION_FAILED, 413 PAYLOAD_TOO_LARGE, 413 REQUEST_ENTITY_TOO_LARGE, 414 URI_TOO_LONG, 414 REQUEST_URI_TOO_LONG, 415 UNSUPPORTED_MEDIA_TYPE, 416 REQUESTED_RANGE_NOT_SATISFIABLE, 417 EXPECTATION_FAILED, 418 I_AM_A_TEAPOT, 419 INSUFFICIENT_SPACE_ON_RESOURCE, 420 METHOD_FAILURE, 421 DESTINATION_LOCKED, 422 UNPROCESSABLE_ENTITY, 423 LOCKED, 424 FAILED_DEPENDENCY, 426 UPGRADE_REQUIRED, 428 PRECONDITION_REQUIRED, 429 TOO_MANY_REQUESTS, 431 REQUEST_HEADER_FIELDS_TOO_LARGE, 451 UNAVAILABLE_FOR_LEGAL_REASONS, 500 INTERNAL_SERVER_ERROR, 501 NOT_IMPLEMENTED, 502 BAD_GATEWAY, 503 SERVICE_UNAVAILABLE, 504 GATEWAY_TIMEOUT, 505 HTTP_VERSION_NOT_SUPPORTED, 506 VARIANT_ALSO_NEGOTIATES, 507 INSUFFICIENT_STORAGE, 508 LOOP_DETECTED, 509 BANDWIDTH_LIMIT_EXCEEDED, 510 NOT_EXTENDED, 511 NETWORK_AUTHENTICATION_REQUIRED)
|**view** +
__可选__|**样例** : `"<<_view>>"`|<<_view,View>>
|**viewName** +
__可选__|**样例** : `"string"`|string
|===


[[_objectnode]]
=== ObjectNode

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**array** +
__可选__|**样例** : `true`|boolean
|**bigDecimal** +
__可选__|**样例** : `true`|boolean
|**bigInteger** +
__可选__|**样例** : `true`|boolean
|**binary** +
__可选__|**样例** : `true`|boolean
|**boolean** +
__可选__|**样例** : `true`|boolean
|**containerNode** +
__可选__|**样例** : `true`|boolean
|**double** +
__可选__|**样例** : `true`|boolean
|**float** +
__可选__|**样例** : `true`|boolean
|**floatingPointNumber** +
__可选__|**样例** : `true`|boolean
|**int** +
__可选__|**样例** : `true`|boolean
|**integralNumber** +
__可选__|**样例** : `true`|boolean
|**long** +
__可选__|**样例** : `true`|boolean
|**missingNode** +
__可选__|**样例** : `true`|boolean
|**nodeType** +
__可选__|**样例** : `"string"`|enum (ARRAY, BINARY, BOOLEAN, MISSING, NULL, NUMBER, OBJECT, POJO, STRING)
|**null** +
__可选__|**样例** : `true`|boolean
|**number** +
__可选__|**样例** : `true`|boolean
|**object** +
__可选__|**样例** : `true`|boolean
|**pojo** +
__可选__|**样例** : `true`|boolean
|**short** +
__可选__|**样例** : `true`|boolean
|**textual** +
__可选__|**样例** : `true`|boolean
|**valueNode** +
__可选__|**样例** : `true`|boolean
|===


[[_result]]
=== Result
API接口的返回对象


[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**data** +
__可选__|返回的数据 +
**样例** : `"object"`|object
|**msg** +
__可选__|返回的详细说明 +
**样例** : `"成功"`|string
|**success** +
__必填__|是否成功 +
**样例** : `true`|boolean
|===


[[_roledto]]
=== RoleDTO

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**createTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**createUser** +
__可选__|**样例** : `"string"`|string
|**id** +
__可选__|**样例** : `"string"`|string
|**menuIdList** +
__可选__|菜单ID集合 +
**样例** : `[ "string" ]`|< string > array
|**menuList** +
__可选__|**样例** : `[ "<<_menuentity>>" ]`|< <<_menuentity,MenuEntity>> > array
|**remark** +
__可选__|**样例** : `"string"`|string
|**roleName** +
__可选__|**样例** : `"string"`|string
|===


[[_roleentity]]
=== RoleEntity

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**createTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**createUser** +
__可选__|**样例** : `"string"`|string
|**id** +
__可选__|**样例** : `"string"`|string
|**menuList** +
__可选__|**样例** : `[ "<<_menuentity>>" ]`|< <<_menuentity,MenuEntity>> > array
|**remark** +
__可选__|**样例** : `"string"`|string
|**roleName** +
__可选__|**样例** : `"string"`|string
|===


[[_rolemenudto]]
=== RoleMenuDTO

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**menuList** +
__必填__|菜单ID集合 +
**样例** : `[ "string" ]`|< string > array
|**roleId** +
__必填__|角色Id +
**样例** : `"string"`|string
|===


[[_d90a08b082c344056003e456e94cdcc3]]
=== SCHEDULE_JOB对象
SCHEDULE_JOB对象


[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**beanName** +
__可选__|bean名称 +
**样例** : `"string"`|string
|**createTime** +
__可选__|创建时间 +
**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**cronExpression** +
__可选__|表达式 +
**样例** : `"string"`|string
|**id** +
__可选__|主键 +
**样例** : `"string"`|string
|**params** +
__可选__|参数 +
**样例** : `"string"`|string
|**remark** +
__可选__|备注 +
**样例** : `"string"`|string
|**status** +
__可选__|状态，0：正常，1：暂停 +
**样例** : `0`|integer (int32)
|===


[[_timestamp]]
=== Timestamp

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**date** +
__可选__|**样例** : `0`|integer (int32)
|**day** +
__可选__|**样例** : `0`|integer (int32)
|**hours** +
__可选__|**样例** : `0`|integer (int32)
|**minutes** +
__可选__|**样例** : `0`|integer (int32)
|**month** +
__可选__|**样例** : `0`|integer (int32)
|**nanos** +
__可选__|**样例** : `0`|integer (int32)
|**seconds** +
__可选__|**样例** : `0`|integer (int32)
|**time** +
__可选__|**样例** : `0`|integer (int64)
|**timezoneOffset** +
__可选__|**样例** : `0`|integer (int32)
|**year** +
__可选__|**样例** : `0`|integer (int32)
|===


[[_userentity]]
=== UserEntity

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**age** +
__可选__|**样例** : `0`|integer (int32)
|**createTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**createUser** +
__可选__|**样例** : `"string"`|string
|**deleteStatus** +
__可选__|**样例** : `0`|integer (int32)
|**email** +
__可选__|**样例** : `"string"`|string
|**id** +
__可选__|**样例** : `"string"`|string
|**mobile** +
__可选__|**样例** : `"string"`|string
|**password** +
__可选__|**样例** : `"string"`|string
|**roleList** +
__可选__|**样例** : `[ "<<_roleentity>>" ]`|< <<_roleentity,RoleEntity>> > array
|**salt** +
__可选__|**样例** : `"string"`|string
|**sex** +
__可选__|**样例** : `"string"`|string
|**status** +
__可选__|**样例** : `0`|integer (int32)
|**updateTime** +
__可选__|**样例** : `"<<_timestamp>>"`|<<_timestamp,Timestamp>>
|**updateUser** +
__可选__|**样例** : `"string"`|string
|**username** +
__可选__|**样例** : `"string"`|string
|===


[[_userroledto]]
=== UserRoleDTO

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**roleList** +
__必填__|角色ID集合 +
**样例** : `[ "string" ]`|< string > array
|**userId** +
__必填__|用户Id +
**样例** : `"string"`|string
|===


[[_view]]
=== View

[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**contentType** +
__可选__|**样例** : `"string"`|string
|===


[[_fea127974cda404395799413a0680320]]
=== 登录表单对象
登录表单对象


[options="header", cols=".^3,.^11,.^4"]
|===
|名称|说明|类型
|**captcha** +
__可选__|验证码 +
**样例** : `"string"`|string
|**password** +
__可选__|密码 +
**样例** : `"string"`|string
|**username** +
__可选__|用户名 +
**样例** : `"string"`|string
|**uuid** +
__可选__|生成验证码的UUID +
**样例** : `"string"`|string
|===



